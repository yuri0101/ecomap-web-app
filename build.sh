echo ----------- Get Latest Changes ---------------
# cd /home/crypto-vue
git stash
git checkout dev
git pull origin dev
echo

echo ----------- Build Project ---------------
cp -n .env.example .env
rm -rf node_modules
npm install
npm run build

echo ----------- Run Project ---------------
pm2 delete "crypto-vue"
pm2 start "/usr/bin/npm" --name "crypto-vue" -- start
