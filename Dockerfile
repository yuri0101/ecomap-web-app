#FROM tobyornottobe/app:b169
FROM node:10.16.0-jessie
# Create app directory
WORKDIR /usr/src/app
RUN rm -rf node_modules dist
COPY . .
RUN npm install -g npm
RUN npm install
RUN npm run generate
EXPOSE 3000
CMD [ "npm", "start" ]

