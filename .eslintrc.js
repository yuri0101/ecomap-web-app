module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/essential'
  ],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  // add your custom rules here
  rules: {
    "semi": ["error", "never"],
    "quotes": ["warn", "single"],
    "comma-dangle": ["warn", "always-multiline"],
    "no-console": ["error", { allow: ["warn", "error", "log"] }],
    'space-before-function-paren': ['warn', {
      'named': 'never',
      'asyncArrow': 'always'
    }]
  }
}
