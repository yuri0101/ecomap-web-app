import { cloneDeep } from 'lodash'
import Vue from 'vue'

export const state = () => ({
  // static
  scaleBy: 1.1,
  markerDefaultConfig: {
    fill: '#FFCAA1',
    textFill: '#FB6E00',
    stroke: '#FFCAA1',
    strokeWidth: 2,
    offsetX: 0,
    offsetY: 0,
    draggable: true,
    angle: 360,
    align: 'left',
    text: 'Type something',
    fontSize: 24,
    lineHeight: 1,
    fontFamily: 'Inter',
    fontStyle: 'normal',
    fontWeight: 'bold',
  },
  markerColors: [
    '#FFCAA1', //#82C341
    '#FFBFB0', //FB6E00
    '#FFE28A', //#2B839D
    '#C1E1A1', //#806BD6
    '#AADDEC', //#FFC700
    '#F8F7F4', //#EC53DD
  ],
  // dev
  selectedMarkerTool: null,
  activeMarker: null,
  activeModule: null,
  moveableStage: false,
  stageScale: 1,
  stagePosition: { x: 0, y: 0 },
  moduleRadius: 50,
  unsaved: false,
  saving: false,
  loading: true,
  // ----------Board data --------- //
  boardList: [],
  activeBoard: null,

  top: 0,
  left: 0,
  zoom: 1,
  logoShape: 'circle',
  links: [],
  modules: [],
  markers: [],
  filteredModules: [],

  // TODO
  selected: null,
  versions: [],
  boardTitle: null,
  boardDetails: null,
  boardId: null,
  boardHash: null,
  boardPublic: false,
  owner: false,
  search: null,
  // configs
  groupHash: 0,
  // data
  groups: {},
  deleteGroup: '',
  autoGenerationBoard: {},
  latestNodeId: null,
  nodeDisplaced: null,
  nodeMoved: false,
  isDragging: false,
  counter: 0,
  textChanged: false,
  samePos: false,
})

export const getters = {
  scaleBy: (state) => state.scaleBy,
  markerDefaultConfig: (state) => state.markerDefaultConfig,
  markerColors: (state) => state.markerColors,
  //
  modules: (state) => state.modules,
  markers: (state) => state.markers,
  activeMarker: (state) => state.activeMarker,
  activeModule: (state) => state.activeModule,
  activeMarkerId: (state) => state.activeMarker?.id,
  selectedMarkerTool: (state) => state.selectedMarkerTool,
  moveableStage: (state) => state.moveableStage,
  stageScale: (state) => state.stageScale,
  stagePosition: (state) => state.stagePosition,
  filteredModules: (state) => state.filteredModules,
  // ------------------------------------------------ //
  top: (state) => state.top,
  left: (state) => state.left,
  zoom: (state) => state.zoom,
  moduleRadius: (state) => state.moduleRadius,
  links: (state) => state.links,
  groups: (state) => state.groups,
  selected: (state) => state.selected,
  activeBoard: (state) => state.activeBoard,
  boardList: (state) => state.boardList,
  boardPublic: (state) => state.boardPublic,
  owner: (state) => state.owner,
  title: (state) => state.boardTitle,
  id: (state) => state.boardId,
  selectedBoard: (state) => state.id,
  details: (state) => state.boardDetails,
  unsaved: (state) => state.unsaved,
  saving: (state) => state.saving,
  loading: (state) => state.loading,
  logoShape: (state) => state.logoShape,
  deleteGroup: (state) => state.deleteGroup,
  boardHash: (state) => state.boardHash,
  autoGenerationBoard: (state) => state.autoGenerationBoard,
  search: (state) => state.search,
  moduleLength: (state) => state.modules.length,
  moved: (state) => state.nodeMoved,
  nodeDisplaced: (state) => state.nodeDisplaced,
}

export const mutations = {
  setInitialState(state) {
    if (state.id) {
      const duplicateState = cloneDeep(state)
      this.state.selectBoardId = state.id
      Vue.set(this.state.redoArray, [state.id], [])
      Vue.set(this.state.undoArray, [state.id], [])
      this.state.undoArray[state.id].push(duplicateState)
    } else {
      const duplicateState = cloneDeep(state)
      this.state.redoArrayNoId = []
      this.state.undoArrayNoId = []
      this.state.undoArrayNoId.push(duplicateState)
    }
  },
  // update any key (not auto saved)
  updateStore(state, data) {
    Object.keys(data).forEach((key) => {
      Vue.set(state, key, data[key])
    })
  },
  // update any key (auto saved)
  updateStoreDetecting(state, data) {
    Object.keys(data).forEach((key) => {
      Vue.set(state, key, data[key])
    })
  },
  // modules
  addModules(state, modules) {
    for (let x of state.modules) {
      for (let node of modules) {
        if (node.id === x.id) {
          Vue.notify.warning({
            title: 'Already exists',
            message: `[${node.name}] is already on board`,
          })
        }
      }
    }
    state.modules = [...state.modules, ...modules]
  },
  tranformModule(state, md) {
    state.activeModule = md
    state.modules = [
      ...state.modules.map((el) => {
        if (md.markerId) {
          if (el.id === md.id && el.markerId === md.markerId)
            return { ...el, ...md }
        } else {
          if (el.id === md.id && !el.markerId) {
            return { ...el, ...md }
          }
        }
        return { ...el }
      }),
    ]
    console.log(state.modules)
  },
  deleteModule(state, id) {
    state.modules = state.modules.filter((v) => v.id !== id)
  },
  // markers
  selectedMarkerTool(state, value) {
    state.selectedMarkerTool = value
    if (value) state.moveableStage = false
  },
  createMarker(state, marker) {
    state.selectedMarkerTool = null
    state.activeMarker = marker
    state.markers = [...state.markers, marker]
  },
  tranformMarker(state, marker) {
    state.activeMarker = marker
    state.markers = [
      ...state.markers.map((el) => {
        if (marker && el.id === marker.id) return { ...el, ...marker }
        else return { ...el }
      }),
    ]
  },
  // not auto save
  transformActiveMarker(state, marker) {
    state.activeMarker = marker
    state.markers = [
      ...state.markers.map((el) => {
        if (marker && el.id === marker.id) return { ...el, ...marker }
        else return { ...el }
      }),
    ]
  },
  duplicateMarker(state, marker) {
    if (!marker.id) return
    const newMarker = { ...marker }
    newMarker.x += 50
    newMarker.y += 50
    newMarker.id = newMarker.type + new Date().getTime()
    state.activeMarker = newMarker
    state.markers = [...state.markers, newMarker]
  },
  deleteMarker(state, id) {
    if (!id) return
    state.activeMarker = null
    state.markers = [...state.markers.filter((el) => el.id !== id)]
  },
  lockMarker(state, lockObj) {
    if (!lockObj.id) return
    state.markers = [
      ...state.markers.map((el) => {
        if (el.id === lockObj.id) return { ...el, draggable: !lockObj.lock }
        else return { ...el }
      }),
    ]
    if (lockObj.lock) {
      state.activeMarker = null
    } else {
      const marker = state.markers.find((el) => el.id === lockObj.id)
      state.activeMarker = marker
    }
  },
  // board
  setGroups(state, groups) {
    Object.keys(groups).forEach((key) => {
      const oldTitle = state.groups[key] && state.groups[key].title
      groups[key].title = groups[key].title || oldTitle || 'Untitled group'
    })
    Vue.set(state, 'groups', groups)
  },
  addBoard(state, board) {
    let boards = state.boardList
    boards.push(board)
    Vue.set(state, 'boardList', boards)
  },
  loadBoard(state, board) {
    if (state.unsaved === false) {
      Vue.set(state, 'top', 0)
      Vue.set(state, 'left', 0)
      Vue.set(state, 'zoom', 1)
      Vue.set(state, 'activeBoard', board)
      Vue.set(state, 'boardTitle', board.title)
      Vue.set(state, 'boardDetails', board.details)
      Vue.set(state, 'selected', board._id)
      Vue.set(state, 'id', board._id)
      Vue.set(state, 'boardPublic', board.status)
      Vue.set(state, 'boardHash', board.hash)
      Vue.set(state, 'versions', board.__v || [])
      Vue.set(state, 'markers', board.markers || [])
      Vue.set(state, 'modules', board.modules || [])
      Vue.set(state, 'logoShape', board.settings.shape || 'circle')

      Vue.set(state, 'links', board.links || [])
      Vue.set(state, 'owner', board.owner)
      Vue.set(state, 'groups', board.groups || {})
      Vue.set(state, 'groupHash', board.config?.groupHash || 0)
    }
  },
  resetBoard(state) {
    state.links = []
    state.groups = []
    state.modules = []
  },
  // ------------------------------------------------------------- //

  addNode(state, node) {
    state.latestNodeId = node.id

    for (let x of state.modules) {
      if (node.id === x.id) {
        Vue.notify.warning({
          title: 'Already exists',
          message: `[${node.name}] is already on board`,
        })
      }
    }
    // state.modules.push(node)
    Vue.set(state, 'modules', [...state.modules, node])

    if (state.id) {
      const duplicateState = cloneDeep(state)
      Vue.set(this.state.redoArray, [state.id], [])
      this.state.undoArray[state.id].push(duplicateState)
    } else {
      const duplicateState = cloneDeep(state)
      this.state.redoArrayNoId = []
      this.state.undoArrayNoId.push(duplicateState)
    }
  },
  addMarker(state, marker) {
    marker.id =
      marker.type + state.markers.length + Math.floor(Math.random() * 10001)
    state.markers.push(JSON.parse(JSON.stringify(marker)))

    if (state.id) {
      const duplicateState = cloneDeep(state)
      Vue.set(this.state.redoArray, [state.id], [])
      this.state.undoArray[state.id].push(duplicateState)
    } else {
      const duplicateState = cloneDeep(state)
      this.state.redoArrayNoId = []
      this.state.undoArrayNoId.push(duplicateState)
    }

    mutations.updateMarkers.bind(this, state, { id: marker.id }, true)()
  },
  updateMarkers(state, data, added = false) {
    console.log('updateMarkers - board', state, data)
    const id = data.id
    const payload = data.payload
    if (!id) return
    state.markers = state.markers.map((item) => {
      if (item.id === id) {
        if (!id.includes('title') && !id.includes('textbox')) {
          if (
            state.isDragging &&
            !payload.hasOwnProperty('scaleX') &&
            item.x === payload.x &&
            item.y === payload.y
          ) {
            state.samePos = true
            console.log('state.isDragging', state.isDragging)
            console.log('item.x === payload.x', item.x === payload.x)
            console.log('item.y === payload.y', item.y === payload.y)
          } else if (
            state.isDragging &&
            !payload.hasOwnProperty('scaleX') &&
            item.x !== payload.x &&
            item.y !== payload.y
          ) {
            state.samePos = false
            console.log('6')
          }
        } else {
          console.log('7')
        }
      }
      if (item.id === id || id === 'all') {
        mutations.setActiveMarker(
          state,
          id === 'all' ? undefined : { ...item, ...payload },
        )
        return {
          ...item,
          ...payload,
        }
      } else {
        item.selected = false
        item.editable = false
      }
      return item
    })

    if (id == 'all') {
      // state.textChanged = false
      return
    }
    if (
      added ||
      state.isDragging ||
      (state.counter === 2 && !state.isDragging) ||
      (!state.counter && !state.textChanged) ||
      (state.samePos && (state.counter === 3 || state.counter === 2))
    ) {
      console.log('added', added)
      console.log('state.isDragging', state.isDragging)
      console.log('state.counter', state.counter)
      console.log('state.textChanged', state.textChanged)
      console.log('state.samePos', state.samePos)
      // } else if ((id.includes('title') || id.includes('textbox')) && !state.textChanged) {
    } else if (!payload) {
      console.log('3')
    } else if (
      payload.hasOwnProperty('selected') ||
      payload.hasOwnProperty('editable')
    ) {
      console.log('4')
    } else {
      if (state.id) {
        const duplicateState = cloneDeep(state)
        Vue.set(this.state.redoArray, [state.id], [])
        this.state.undoArray[state.id].push(duplicateState)
      } else {
        const duplicateState = cloneDeep(state)
        console.log(this.state)
        this.state.redoArrayNoId = []
        this.state.undoArrayNoId.push(duplicateState)
      }
    }
  },
  updateSearch(state, keyword) {
    state.search = keyword
  },
  setActiveMarker(state, data) {
    Vue.set(state, 'activeMarker', data)
  },
  updateActiveMarker(state, data) {
    const newActiveMarker = Object.assign({}, state.activeMarker)
    newActiveMarker.styles[data.type] = data.value
    mutations.setActiveMarker(state, newActiveMarker)
  },
  addLink(state, data) {
    state.links.push(data)

    if (state.id) {
      const duplicateState = cloneDeep(state)
      Vue.set(this.state.redoArray, [state.id], [])
      this.state.undoArray[state.id].push(duplicateState)
    } else {
      const duplicateState = cloneDeep(state)
      this.state.redoArrayNoId = []
      this.state.undoArrayNoId.push(duplicateState)
    }
  },
  initGroup(state, data) {
    state.groups = data
  },
  updateNode(state, data) {
    const id = data.id
    state.nodeDisplaced = id
    let canUndo = true
    if (!id) return
    for (let node of state.modules) {
      if (node.id !== data.id) continue
      if (data.position) {
        if (
          node.position.x === data.position.x &&
          node.position.y === data.position.y
        ) {
          canUndo = false
          state.nodeMoved = false
          continue
        }
        state.nodeMoved = true
      }
      Object.keys(data).forEach((k) => {
        Vue.set(node, k, data[k])
      })
      break
    }

    if (canUndo) {
      if (state.id) {
        const duplicateState = cloneDeep(state)
        Vue.set(this.state.redoArray, [state.id], [])
        this.state.undoArray[state.id].push(duplicateState)
      } else {
        const duplicateState = cloneDeep(state)
        this.state.redoArrayNoId = []
        this.state.undoArrayNoId.push(duplicateState)
      }
    }
  },
  deleteNode(state, id) {
    // remove node
    const modules = state.modules.filter((v) => v.id !== id)
    Vue.set(state, 'modules', modules)
    // detach links
    const links = state.links.filter((v) => {
      return v.sourceElem !== `node${id}` && v.targetElem !== `node${id}`
    })
    Vue.set(state, 'links', links)

    if (state.id) {
      const duplicateState = cloneDeep(state)
      Vue.set(this.state.redoArray, [state.id], [])
      this.state.undoArray[state.id].push(duplicateState)
    } else {
      const duplicateState = cloneDeep(state)
      this.state.redoArrayNoId = []
      this.state.undoArrayNoId.push(duplicateState)
    }
  },
  detachLink(state, { source, target }) {
    console.log('detachLink')
    const links = state.links.filter((v) => {
      return v.source !== source || v.target !== target
    })
    Vue.set(state, 'links', links)

    if (state.id) {
      const duplicateState = cloneDeep(state)
      Vue.set(this.state.redoArray, [state.id], [])
      this.state.undoArray[state.id].push(duplicateState)
    } else {
      const duplicateState = cloneDeep(state)
      this.state.redoArrayNoId = []
      this.state.undoArrayNoId.push(duplicateState)
    }
  },
  incrementGroupHash(state) {
    state.groupHash++
  },
  decrementGroupHash(state) {
    state.groupHash--
  },
  updateGroupTitle(state, data) {
    Object.keys(data).forEach((key) => {
      const title = data[key]
      Vue.set(state.groups[key], 'title', title)
    })
  },

  updateDeleteGroup(state, value) {
    Vue.set(state, 'deleteGroup', value)
  },
  setAutoGenerationBoard(state, { data, title, autoGenerationMap }) {
    let modules = []
    let markers = []
    data = data.filter((map) => {
      return map.modules.length > 0
    })
    data.forEach((map, i) => {
      if (!autoGenerationMap.groups.hasOwnProperty(i)) {
        return
      }
      let position = autoGenerationMap.groups[i].position
      map.modules.forEach((module, j) => {
        let pos = {}
        let row = 0
        if (j > 5) {
          return
        }
        if (j > 2) {
          row = 1
        }
        pos.x = position.x + 160 * (j % 3)
        pos.y = position.y + 160 * row
        modules.push({
          id: module.module_id,
          position: pos,
        })
      })

      // Deep Clone
      let marker = JSON.parse(JSON.stringify(autoGenerationMap.marker))
      marker.styles.editableText = map.industry.title
      marker.x = position.x
      marker.y = position.y - 140
      markers.push(marker)
    })

    let c = document.createElement('canvas')
    let ctx = c.getContext('2d')
    ctx.font = '40px Arial'
    let txt_width = ctx.measureText(title).width

    let markerCenter = autoGenerationMap.markerCenter
    markerCenter.width = txt_width
    markerCenter.styles.editableText =
      title + '\n' + autoGenerationMap.titleCenter
    markers.push(autoGenerationMap.markerCenter)

    let autoGenerationBoard = {
      title: title,
      details: autoGenerationMap.details,
      private: 1,
      modules: JSON.stringify(modules),
      markers: JSON.stringify(markers),
    }
    Vue.set(state, 'autoGenerationBoard', autoGenerationBoard)
  },
  isDragging(state, data) {
    state.isDragging = data
  },
  incrementCtr(state) {
    state.counter += 1
  },
  resetCtr(state) {
    state.counter = 0
  },
  setTextChanged(state, data) {
    state.textChanged = data
  },
  checkMarkerPos(state) {
    if (!state.isDragging && state.counter === 2) {
      state.samePos = false
    }
  },
  updateFilteredModules(state, modules) {
    Vue.set(state, 'filteredModules', modules)
  },
}

export const actions = {
  // ------------------------------------------------------------- //

  addMarker({ commit }, marker) {
    commit('addMarker', marker)
  },
  updateMarkers({ commit }, data) {
    commit('updateMarkers', data)
  },
  updateSearch({ commit }, keyword) {
    commit('updateSearch', keyword)
  },
  updatePosition({ commit }, data) {
    commit('updateNode', data)
  },
  connect({ commit }, data) {
    commit('addLink', data)
  },
  disconnect({ commit }, data) {
    commit('detachLink', data)
  },
  deleteNode({ commit }, id) {
    commit('deleteNode', id)
  },
  createGroup({ state, commit, dispatch }, bound) {
    if (!bound) return
    commit('incrementGroupHash')
    var selected = false
    const group = `group-${state.groupHash}`
    for (const node of state.modules) {
      if (!node.position) continue
      if (
        node.position.x < bound.x1 ||
        node.position.y < bound.y1 ||
        node.position.x + 40 > bound.x2 ||
        node.position.y + 40 > bound.y2
      ) {
        continue
      }
      commit('updateNode', {
        id: node.id,
        group,
      })
      selected = true
    }
    if (selected) {
      dispatch('rebuildGroups')
    }
    return selected
  },
  deleteGroupContainer({ state, commit }, groupHash) {
    if (!groupHash) return
    // commit('decrementGroupHash')
    const group = undefined
    for (const node of state.modules) {
      if (!node.group) continue
      if (node.group === undefined) {
        continue
      }
      commit('updateNode', {
        id: node.id,
        group,
      })
    }
  },

  rebuildGroups({ state, commit }) {
    // TODO
    const minVal = 0
    const maxVal = Number.MAX_SAFE_INTEGER
    const groups = {}
    for (const node of state.modules) {
      if (!node.group || !node.position) continue
      const bound = groups[node.group] || {
        top: maxVal,
        left: maxVal,
        right: minVal,
        bottom: minVal,
      }
      bound.top = Math.min(bound.top, node.position.y)
      bound.left = Math.min(bound.left, node.position.x)
      bound.right = Math.max(bound.right, node.position.x)
      bound.bottom = Math.max(bound.bottom, node.position.y)
      groups[node.group] = bound
    }
    console.log('groups =>', groups)
    commit('setGroups', groups)
  },

  load({ commit, dispatch, state }, board) {
    commit('loadBoard', board || state || {})
    return dispatch('rebuildGroups')
  },
}
