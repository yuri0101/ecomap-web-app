import Vue from 'vue'

export const state = () => ({
  types: null,
  modules: {},
  selected: null,
})

export const getters = {
  get: state => state.types || [],
  selected: state => state.selected,
  modules: state => state.modules || {},
}

export const mutations = {
  set(state, list) {
    Vue.set(state, 'types', list)
  },
  SELECT(state, typeName) {
    Vue.set(state, 'selected', typeName)
  },
  SET_MODULES(state, list) {
    Vue.set(state.modules, state.selected, list || [])
  },
  PUSH_MODULES(state, list) {
    if (!state.modules[state.selected]) {
      Vue.set(state.modules, state.selected, [])
    }
    state.modules[state.selected].push(...(list || []))
  },
  REMOVE_MODULE(state, nodeId) {
    const list = state.modules[state.selected] || []
    for (let i = 0; i < list.length; i++) {
      if (list[i].id === nodeId) {
        list.splice(i, 1)
        break
      }
    }
  },
  UPDATE_MODULE(state, node) {
    const list = state.modules[state.selected] || []
    for (const item of list) {
      if (item.id === node.id) {
        item.name = node.name
        item.image = node.image
        break
      }
    }
  },
}
