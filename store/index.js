import Vue from 'vue'
import { clone, cloneDeep } from 'lodash'

export const strict = false

// export const plugins = [ undRedo({ 
//   paths: [ 
//     { 
//       namespace: 'board',
//       ignoreMutations: ['addMarker', 'updateMarkers', 'updateSearch', 'duplicateMarker', 'removeMarker', 'setActiveMarker', 'updateActiveMarker', 'initGroup', 'incrementGroupHash', 'decrementGroupHash', 'updateGroupTitle', 'saving', 'loading', 'setBoardList', 'setGroups', 'updateStore','addBoard']
//     }
//   ]
// })]

export const state = () => ({
  // ------------------------ //
  routerLoading:'',
  addModule: null,
  addType: null,
  loginModal: null,
  paymentModal: null,
  introVideoModal:null,
  infoModal: null,
  infoPage:null,
  smallModal:null,
  smallIn:null,
  suggestModal:null,
  editModal: null,
  embedModal: null,
  addBoardModal: null,
  registerModal: null,
  stakeholderInfoModal: null,
  resetModal: null,
  contactModal: null,
  autoGenerationFormModal: null,
  autoGenerationPaymentModal: null,
  autoGenerationEmptyModal: null,
  autoGenerationAnimationModal: null,
  stripePaymentModal: null,
  paymentDetails: {
    type: null,
    status: null,
  },
  prices:{
    oneTime: null,
    Premium: null,
    Enterprise: null,
  },
  passwordChangeModal: null,
  sidebarVisible: false,
  sidebarLeftVisible: false,
  marketMapInfoVisible: true,
  enableSelection: false,
  expandedBoardList: true,
  toggleBoardList: true,
  tourModal: false,
  duplicateConfirmationModal: false,
  activeTab: 0,
  logoShape: 'circle',
  autogenerationMapData:{},
  undoArray: {},
  redoArray: {},
  undoArrayNoId: [],
  redoArrayNoId: [],
  selectBoardId: null,
})

export const getters = {
  addModuleModal: state => state.addModule,
  // ---------------------------------------------------- //
  routerLoading: state => state.routerLoading,
  addTypeModal: state => state.addType,
  infoModal: state => state.infoModal,
  infoPage: state => state.infoPage,
  smallModal: state => state.smallModal,
  smallIn: state => state.smallIn,
  suggestModal: state => state.suggestModal,
  editModal: state => state.editModal,
  loginModal: state => state.loginModal,
  paymentModal: state => state.paymentModal,
  introVideoModal: state => state.introVideoModal,
  registerModal: state => state.registerModal,
  resetModal: state => state.resetModal,
  embedModal: state => state.embedModal,
  contactModal: state => state.contactModal,
  autoGenerationFormModal: state => state.autoGenerationFormModal,
  autoGenerationPaymentModal: state => state.autoGenerationPaymentModal,
  autoGenerationEmptyModal: state => state.autoGenerationEmptyModal,
  autoGenerationAnimationModal: state => state.autoGenerationAnimationModal,
  stripePaymentModal: state => state.stripePaymentModal,
  getPaymentDetails: state => state.paymentDetails,
  getPrices: state => state.prices,
  passwordChangeModal: state => state.passwordChangeModal,
  addBoardModal: state => state.addBoardModal,
  stakeholderInfoModal: state => state.stakeholderInfoModal,
  sidebarVisible: state => state.sidebarVisible,
  sidebarLeftVisible: state => state.sidebarLeftVisible,
  marketMapInfoVisible: state => state.marketMapInfoVisible,
  enableSelection: state => state.enableSelection,
  expandedBoardList: state => state.expandedBoardList,
  toggleBoardList: state => state.toggleBoardList,
  tourModal: state => state.tourModal,
  duplicateConfirmationModal: state => state.duplicateConfirmationModal,
  activeTab: state => state.activeTab,
  logoShape: state => state.logoShape,
  getAutogenerationMapData: state => state.autogenerationMapData,
  totalChanges: state => {
    if(!state.selectBoardId) return 0
    return state.undoArray[state.selectBoardId].length - 1
  },
  redoArrayLength(state) {
    if(state.selectBoardId) return state.redoArray[state.selectBoardId].length
    else return 0
  },
  redoArrayNoIdLength(state) {
    return state.redoArrayNoId.length
  },
  undoArrayLength(state) {
    if(state.selectBoardId) return state.undoArray[state.selectBoardId].length
    else return 0
  },
  undoArrayNoIdLength(state) {
    return state.undoArrayNoId.length
  },
}

export const mutations = {
  // ------------------------------------------ //
  setRouterLoading(state, payload) {
    state.routerLoading = payload
  },
  showLoginModal(state, value) {
    Vue.set(state, 'loginModal', value)
  },
  showPaymentModal(state, value) {
    Vue.set(state, 'paymentModal', value)
  },
  showIntroVideoModal(state, value) {
    Vue.set(state, 'introVideoModal', value)
  },
  showRegisterModal(state, value) {
    Vue.set(state, 'registerModal', value)
  },
  showAddType(state, value) {
    Vue.set(state, 'addType', value)
  },
  showResetModal(state, value) {
    Vue.set(state, 'resetModal', value)
  },
  showEmbedModal(state, value) {
    Vue.set(state, 'embedModal', value)
  },
  showContactModal(state, value) {
    Vue.set(state, 'contactModal', value)
  },
  showAutoGenerationFormModal(state, value) {
    Vue.set(state, 'autoGenerationFormModal', value)
  },
  showAutoGenerationPaymentModal(state, value) {
    Vue.set(state, 'autoGenerationPaymentModal', value)
  },
  showAutoGenerationEmptyModal(state, value) {
    Vue.set(state, 'autoGenerationEmptyModal', value)
  },
  showAutoGenerationAnimationModal(state, value) {
    Vue.set(state, 'autoGenerationAnimationModal', value)
  },
  showStripePaymentModal(state, value) {
    Vue.set(state, 'stripePaymentModal', value)
  },
  setPaymentDetails(state, value) {
    Vue.set(state, 'paymentDetails', value)
  },
  setPrices(state, value) {
    Vue.set(state, 'prices', value)
  },
  showPasswordChangeModal(state, value) {
    Vue.set(state, 'passwordChangeModal', value)
  },
  showAddModule(state, value) {
    Vue.set(state, 'addModule', value)
  },
  showInfoModal(state, value) {
    Vue.set(state, 'infoModal', value)
  },
  getInfoPage(state, value) {
    Vue.set(state, 'infoPage', value)
  },
  showSmallModal(state, value) {
    Vue.set(state, 'smallModal', value)
  },
  SmallInBoard(state, value) {
    Vue.set(state, 'smallIn', value)
  },
  showSuggestModal(state, value) {
    Vue.set(state, 'suggestModal', value)
  },
  showEditModal(state, value) {
    Vue.set(state, 'editModal', value)
  },
  showBoardModal(state, value) {
    Vue.set(state, 'addBoardModal', value)
  },
  showStakeholderInfoModal(state, value) {
    Vue.set(state, 'stakeholderInfoModal', value)
  },
  enableSelection(state, value) {
    Vue.set(state, 'enableSelection', value)
  },
  expandedBoardList(state, value) {
    Vue.set(state, 'expandedBoardList', value)
  },
  toggleBoardList(state, value) {
    Vue.set(state, 'toggleBoardList', value)
  },
  sidebarVisible(state, value) {
    Vue.set(state, 'sidebarVisible', value)
  },
  sidebarLeftVisible(state, value) {
    console.log('sidebarLeftVisible')
    Vue.set(state, 'sidebarLeftVisible', value)
  },
  marketMapInfoVisible(state, value) {
    Vue.set(state, 'marketMapInfoVisible', value)
  },
  tourModal(state, value) {
    Vue.set(state, 'tourModal', value)
  },
  enableDuplicateConfirmationModal(state, value) {
    Vue.set(state, 'duplicateConfirmationModal', value)
  },
  updateActiveTab(state, value) {
    Vue.set(state, 'activeTab', value)
  },
  updateLogoShape(state, value) {
    Vue.set(state, 'logoShape', value)
  },
  setAutogenerationMapData(state, value) {
    Vue.set(state, 'autogenerationMapData', value)
  },
  undoBoard(state) {
    if(state.undoArray[this.state.board.id].length > 1) {
      const latestState = state.undoArray[this.state.board.id].pop()
      state.redoArray[this.state.board.id].push(cloneDeep(latestState))
      let lastState = state.undoArray[this.state.board.id][state.undoArray[this.state.board.id].length - 1]
      lastState = cloneDeep(lastState)
      // this.replaceState({
      //   ...this.state,
      //   board: lastState,
      // })
      this.state.board = lastState
    }
  },
  undoBoardNoId(state) {
    if(state.undoArrayNoId.length > 1) {
      const latestState = state.undoArrayNoId.pop()
      state.redoArrayNoId.push(cloneDeep(latestState))
      let lastState = state.undoArrayNoId[state.undoArrayNoId.length - 1]
      lastState = cloneDeep(lastState)
      // this.replaceState({
      //   ...this.state,
      //   board: lastState,
      // })
      this.state.board = lastState
    }
  },
  redoBoard(state) {
    if(state.redoArray[this.state.board.id].length > 0) {
      const nextState = cloneDeep(state.redoArray[this.state.board.id].pop())
      state.undoArray[this.state.board.id].push(cloneDeep(nextState))
      // this.replaceState({
      //   ...this.state,
      //   board: nextState,
      // })
      this.state.board = nextState
    }
  },
  redoBoardNoId(state) {
    if(state.redoArrayNoId.length > 0) {
      const nextState = cloneDeep(state.redoArrayNoId.pop())
      state.undoArrayNoId.push(cloneDeep(nextState))
      // this.replaceState({
      //   ...this.state,
      //   board: nextState,
      // })
      this.state.board = nextState
    }
  },
  resetUndoRedo(state) {
    state.redoArrayNoId = []
    const initUndoArrayNoIdState = cloneDeep(state.undoArrayNoId.pop())
    state.undoArrayNoId = []
    Vue.set(state.undoArrayNoId, 0, initUndoArrayNoIdState)
  },
}

export const actions = {
  toggleSidebar({ state, commit }) {
    commit('sidebarVisible', !state.sidebarVisible)
  },
  toggleSidebarLeft({ state, commit }) {
    console.log('toggleSidebarLeft')
    commit('sidebarLeftVisible', !state.sidebarLeftVisible)
  },
}

