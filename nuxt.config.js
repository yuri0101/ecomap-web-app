require('dotenv').config()

module.exports = {
  components: true,
  mode:'universal',
  devtools: true,
  ssr: false,
  /* Headers of the page */
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'theme-color', content:'#ffffff' },
      { hid: 'description', name: 'description', content: 'Ecomap is a platform to display and learn about ecosystems, industry networks or organisational structures.' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/icons/favicon.ico' },
      { rel: 'apple-touch-icon', sizes:'57x57', href: '/icons/apple-icon-57x57.png' },
      { rel: 'apple-touch-icon', sizes:'60x60', href: '/icons/apple-icon-60x60.png' },
      { rel: 'apple-touch-icon', sizes:'72x72', href: '/icons/apple-icon-72x72.png' },
      { rel: 'apple-touch-icon', sizes:'76x76', href: '/icons/apple-icon-76x76.png' },
      { rel: 'apple-touch-icon', sizes:'114x114', href: '/icons/apple-icon-114x114.png' },
      { rel: 'apple-touch-icon', sizes:'120x120', href: '/icons/apple-icon-120x120.png' },
      { rel: 'apple-touch-icon', sizes:'144x144', href: '/icons/apple-icon-144x144.png' },
      { rel: 'apple-touch-icon', sizes:'152x152', href: '/icons/apple-icon-152x152.png' },
      { rel: 'apple-touch-icon', sizes:'180x180', href: '/icons/apple-icon-180x180.png' },
      { rel: 'icon', type:'image/png', sizes:'32x32', href: '/icons/favicon-32x32.png' },
      { rel: 'icon', type:'image/png', sizes:'96x96', href: '/icons/favicon-96x96.png' },
      { rel: 'icon', type:'image/png', sizes:'16x16', href: '/icons/favicon-16x16.png' },
      // { rel: 'icon', type:'image/png', sizes:'192x192', href: '/icons/android-icon-192x192.png' }, 
      { href: 'https://fonts.googleapis.com/css?family=Inter', rel: 'stylesheet' },
    ],
  },
  /* Configurations */
  loading: { color: '#3B8070' },
  loadingIndicator: {
    name: 'fading-circle',
    color: '#3B8070',
    background: '#f3f3f6',
  },
  // loading: false,
  // loading: '~/components/LoadingG.vue',
  vuetify: {
    icons: {
      iconfont: 'mdi'
    }
    // Vuetify options
    //  theme: { }
  },
  vue: {
    config: {
      productionTip: false,
      devtools: true,
    }
  },
  axios: {
    baseUrl: process.env.API_URL_V3
  },
  auth: {
    redirect: {
      home: false,
      login:'/auth/login',
      logout: false,
    },
    strategies: {
      local: {
        endpoints: {
          login: { url: '/auth/login', method: 'post', propertyName: 'data' },
          logout: { url: '/auth/logout', method: 'post' },
          user: { url: '/user', method: 'get', propertyName: 'data' },
        },
      },
    },
  },
  /* Source Files */
  env: {
    stripeKey: process.env.STRIPE_KEY,
    apiBase: process.env.API_BASE,
    loginUrl: process.env.LOGIN_URL,
  },
  css: [
    // '@/assets/styles/material-icons.scss',
    'izitoast/dist/css/iziToast.min.css',
  ],
  plugins: [
    './plugins/jquery.js',
    // { src: '~/plugins/store-persist.js', ssr: false },
    { src: './plugins/vuekonva.js', ssr: false },
    { src: './plugins/board-persist.js', ssr: false },
    { src: './plugins/loading.js', ssr: false },
    { src: './plugins/validator.js', ssr: false },
    { src: './plugins/notifications.js', ssr: false },
    { src: './plugins/pure-swipe.js', ssr: false },
    { src: './plugins/stripe.js', ssr: false },
    { src: './plugins/auto-logout.js', ssr: false },
    { src: './plugins/init.js', ssr: false },
    { src: './plugins/axios.js', ssr: false },
    { src: './plugins/dev.js', ssr: false },
  ],
  modules: [
    '@nuxtjs/sentry',
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/pwa',
    ['@nuxtjs/vuetify'],
  ],
  pwa:{
    workbox:{
      enabled: true,
      config:{
        debug: true
      }
    },
    manifest: {
      name: 'Ecomap',
      lang: 'en',
      start_url:'/',
      theme_color:'#fb6e00',
      useWebmanifestExtension: false
    }
  },
  sentry:{
    dsn: process.env.SENTRY_DSN,
    config: {
      environment: process.env.NODE_ENV
    },
  },
  build: {
    analyze: true,
    // parallel: true,
    // optimizeCSS: true,
    extend(config, ctx) {
      config.resolve.alias['vue'] = 'vue/dist/vue.common'      
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },
}
