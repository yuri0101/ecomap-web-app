import Vue from 'vue'
import debounce from 'lodash.debounce'

class Board {
  constructor(context) {
    this.busy = false
    this.context = context
    this.router = context.app.router
    this.$store = context.store
    this.$axios = context.$axios
    this.autosave = true
    this._watchStore()
  }

  isShared() {
    return this.$route.query.share !== undefined
  }

  freedomFighter() {
    const $this = this
    return new Promise((resolve) => {
      while ($this.busy) resolve({})
    })
  }

  async _clear() {
    this.$store.dispatch('board/load', null)
  }

  async list() {
    try {
      const result = await this.$axios.$get('/boards')
      console.log('boards list => ', result.data)
      this.$store.commit('board/updateStore', {
        boardList: result.data,
      })
    } catch (err) {
      console.log('Failed to get all boards => ', err)
    }
  }

  async load(params) {
    try {
      if (this.busy) return // await this.freedomFighter()

      if (!params.hash) return this._clear()

      this.busy = true
      this.$store.commit('board/updateStore', {
        loading: true,
      })

      let result = await this.$axios.$get('/board/view', { params })
      console.log('board => ', result.data)

      const board = await this.assignModulesData(result.data)
      await this.$store.dispatch('board/load', board)

      // const tags = await this.$axios.$get('/tag', { q: 'all' })
      // console.log('tags ---------->', tags)
      // const industries = await this.$axios.$get('/industry', { q: 'all' })
      // console.log('industry ---------->', industries)
    } finally {
      this.busy = false
      this.$store.commit('board/updateStore', {
        loading: false,
      })
    }
  }

  async create(params) {
    try {
      if (this.busy) return // await this.freedomFighter()

      this.busy = true
      this.$store.commit('board/updateStore', {
        saving: true,
      })

      const result = await this.$axios.$post('/board/create', params)
      const board = result.data

      const boards = [...this.$store.state.board.boardList, board.board]
      this.$store.commit('board/updateStore', {
        boardList: boards,
      })
      return board
    } finally {
      this.busy = false
      this.$store.commit('board/updateStore', {
        saving: false,
      })
    }
  }

  async save(state, data) {
    try {
      console.log(this.busy)
      if (this.busy) return // await this.freedomFighter()

      this.busy = true
      this.$store.commit('board/updateStore', {
        saving: true,
      })

      let params = this.getCurrentBoardData(state, data)
      console.log('Saving params to /board/store ===> ', params)

      let result = {}

      try {
        result = await this.$axios.$post('/board/store', params) // ?include=board
        console.log('Saved result from /board/store ===> ', result.data)
      } catch (e) {
        if (e.response && e.response.status === 400) {
          // console.log('Log in error...')
          this.$store.commit('board/updateStore', {
            unsaved: true,
          })
        }
        if (e.response && e.response.status === 451) {
          this.$store.commit('board/updateStore', {
            unsaved: true,
          })
        }
        return
      }

      this.$store.commit('board/updateStore', {
        unsaved: false,
      })

      // TODO
      // const board = await this.assignModulesData(result.data)
      // const boards = this.$store.state.board.boardList.map((item) => {
      //   return item.hash === board.hash ? board.board : item
      // })

      // if (params.hash === board.hash) {
      //   this.$store.commit('board/updateStore', {
      //     boardList: boards,
      //     activeBoard: board,
      //     selected: board.board.id,
      //     versions: board.board.versions,
      //     boardPublic: board.board.public,
      //     boardDetails: board.board.details,
      //     boardTitle: board.board.title,
      //   })
      // }
      // return board.board
    } finally {
      this.busy = false
      this.$store.commit('board/updateStore', {
        saving: false,
      })
    }
  }

  async duplicateBoard(state, data) {
    try {
      if (this.busy) return // await this.freedomFighter()

      this.busy = true
      this.$store.commit('board/updateStore', {
        saving: true,
      })

      let params = this.getCurrentBoardData(state, data)
      params.hash = null
      params.title += ' (copy)'

      let result = {}

      try {
        result = await this.$axios.$post('/board/store?include=board', params)
      } catch (e) {
        return
      }

      // TODO
      const board = result.data
      const boards = [...this.$store.state.board.boardList, board.board]

      this.$store.commit('board/addBoard', board.board)
      this.$store.commit('board/updateStore', {
        boardList: boards,
        selected: board.board.id,
        versions: board.board.versions,
        boardPublic: board.board.public,
        boardDetails: board.board.details,
        boardTitle: board.board.title,
      })
      return board.board
    } finally {
      this.busy = false
      this.$store.commit('board/updateStore', {
        saving: false,
      })
    }
  }

  async saveAfterLogin(state, newBoard = false) {
    try {
      this.$store.commit('board/updateStore', {
        saving: true,
      })
      // TODO
      let params = {
        title: state.boardTitle,
        details: state.boardDetails,
        private: state.boardPublic ? 0 : 1,
        hash: newBoard ? '' : state.boardHash,
        links: state.links,
        groups: state.groups,
        configs: {
          groupHash: state.groupHash,
        },
        modules: state.modules,
        markers: state.markers,
      }

      let result = {}

      try {
        result = await this.$axios.$post('/board/store?include=board', params)
      } catch (e) {
        if (e.response && e.response.status === 451) {
          const newBoard = await this.saveAfterLogin(state, true)
          window.location.href = '/' + newBoard.hash
          return newBoard
        }
        return null
      }

      this.$store.commit('board/updateStore', {
        unsaved: false,
      })

      const board = await this.assignModulesData(result.data)
      // TODO
      await this.$store.commit('board/updateStore', board)

      return board
    } finally {
      this.$store.commit('board/updateStore', {
        saving: false,
      })
    }
  }

  async delete(hash) {
    let params = { hash }
    await this.$axios.$get('/board/delete', { params })
  }

  // TODO
  getCurrentBoardData(state, data) {
    const previousBoardData = this.$store.state.board
    const title =
      (data && data.title) ||
      (previousBoardData && previousBoardData.boardTitle) ||
      ''
    let details =
      (data && data.details) ||
      (previousBoardData && previousBoardData.boardDetails) ||
      ''
    let privateBoard = data && data.public ? 0 : 1
    if (data.details === undefined) {
      details = previousBoardData.boardDetails
    }
    if (data.public === undefined) {
      privateBoard = previousBoardData && previousBoardData.boardPublic ? 0 : 1
    }
    state = state || this.$store.state.board
    const modules = data.modules ? data.modules : state.modules
    const markers = data.markers ? data.markers : state.markers
    return {
      title,
      details,
      private: privateBoard,
      hash: state.boardHash,
      shape: state.logoShape,
      links: state.links,
      groups: state.groups,
      configs: {
        groupHash: state.groupHash,
      },
      modules: modules.map((md) => {
        delete md.logo
        return md
      }),
      markers: markers.map((md) => {
        delete md.background
        return md
      }),
    }
  }

  // TODO
  async assignModulesData(data) {
    let promises = []
    /* data.modules = []
    if (data?._data) {
      data.markers = data._data.markers || []
      data.links = data._data.links || []
      data.groups = data._data.groups || {}
      // data.modules = data._data.modules || []
      if (data._data.modules) {
        data._data.modules.forEach((md, index) => {
          promises.push(
            this.$axios.$get('/module/info?id=' + md.id).then((res) => {
              data.modules.push({ ...data._data.modules[index], ...res.data })
            }),
          )
        })
      }
    } */
    return Promise.all(promises).then(() => {
      return data
    })
  }

  _watchStore() {
    const ignoreList = [
      'updateStore',
      'transformActiveMarker',
      'loadBoard',
      'updateFilteredModules',
      'setGroups',
      'setBoardList',
      'saving',
      'loading',
      'store',
    ]

    const $this = this

    const subscriber = debounce(({ type }, state) => {
      console.log('action => ', type)

      if (!$this.autosave) return

      if (!type.startsWith('board/')) return

      const name = type.substr('board/'.length)
      if (ignoreList.indexOf(name) >= 0) return

      if (state.board.selected) {
        this.save(null, state.board)
      }
    }, 1000)

    this.$store.subscribe(subscriber)
  }
}

export default async (context, inject) => {
  const board = new Board(context)
  Vue.prototype.$board = board
  inject('board', board)
}
