export default function ({ $axios, redirect }) {
  $axios.onRequest((config) => {
    console.log('requesting...', config.url)
  })

  $axios.onError((error) => {
    const code = parseInt(error.response && error.response.status)
    const path = error.config.url
    // if not exist token redirect to login
    const bearerToken = localStorage.getItem('auth._token.local')
    if (!bearerToken || bearerToken === 'false') {
      localStorage.clear()
      redirect('/login')
    } else {
      // api error
      console.log('API error ===> ', path, code)
    }
  })
}
