import createPersistedState from 'vuex-persistedstate'

export default ({store}) => {
  createPersistedState({
    key: 'connecto_v2',
    paths: [
      // 'board.selected',
      // 'board.boardHash',
      // 'board.modules',
      // 'board.links',
      // 'board.groupHash'
    ],
  })(store)
}
