const MAX_IDLE = 96 * 3600 * 1000 // old 10 * 60 * 1000

export default async ({ store, app }) => {
  let lastAction = Date.now()

  function resetTime() {
    lastAction = Date.now()
  }

  window.addEventListener('keydown', resetTime)
  window.addEventListener('mousemove', resetTime)
  window.addEventListener('mousedown', resetTime)
  window.addEventListener('touchmove', resetTime)
  window.addEventListener('touchstart', resetTime)
  window.addEventListener('pointermove', resetTime)

  window.setInterval(async () => {
    try {
      if (!store.state.auth.loggedIn) return
      let diff = Date.now() - lastAction
      if (diff < MAX_IDLE) return
      await app.$auth.logout()
      localStorage.clear()
      app.$router.push('/login')
    } catch (e) {
    }
    window.iziToast.warning({
      title: 'Hey',
      message: 'Are you feeling sleepy or have you forgotten me',
      position: 'center',
      timeout: 4800 * 3600 * 1000, // old 2400 * 3600 * 1000,
      overlay: true,
      progressBar: false,
      buttons: [
        [
          '<button>Login</button>',
          (instance, toast) => {
            store.commit('showLoginModal', true)
            window.iziToast.hide({ transitionOut: 'fadeOut' }, toast, 'button')
          },
          true,
        ],
      ],
    })
  }, 10 * 1000)
}
