export default async ({store, $axios}) => {
  window.store = store
  await $axios.$get('/types').then(json => {
    console.log('type/set => ', json.data)
    store.commit('types/set', json.data)
  })
}
