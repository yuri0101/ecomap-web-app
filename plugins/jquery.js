import Vue from 'vue'
import jquery from 'jquery'

if (process.browser) {
  window.$ = window.jQuery = jquery
}

Vue.prototype.$ = Vue.$ = jquery
Vue.prototype.jquery = Vue.jquery = jquery

