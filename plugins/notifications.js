import Vue from 'vue'
import iziToast from 'izitoast'
// import VueNotifications from 'vue-notifications'

export default () => { // { app }, inject
  iziToast.settings({
    timeout: 5000,
    position: 'topCenter',
    theme: 'light',
    displayMode: 'replace',
  })

  window.iziToast = iziToast
  Vue.notify = Vue.prototype.$notify = iziToast
}
