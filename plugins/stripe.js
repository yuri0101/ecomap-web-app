import Vue from 'vue'

function addScript() {
  const script = document.createElement('script')
  script.src = 'https://checkout.stripe.com/checkout.js'
  script.id = 'stripe_checkout'
  document.getElementsByTagName('head')[0].appendChild(script)
}

// function removeScript() {
//   const head = document.getElementsByTagName('head')[0]
//   head.removeChild(document.getElementById('stripe_checkout'))
//   delete window.StripeCheckout
// }

export default async ({ env }) => {
  addScript()

  const options = {
    key: env.stripeKey,
    image: '~/static/logo2.png',
    name: 'Ecomap',
    locale: 'auto',
    // billingAddress: true,
    panelLabel: 'Subscribe @ {{amount}}',
  }

  Vue.prototype.$checkout = {
    async open(opts) {
      if (!window.StripeCheckout) {
        await new Promise(resolve => {
          let counter = 0
          const handler = setInterval(() => {
            counter += 50
            if (window.StripeCheckout || counter > opts.timeout) {
              resolve()
              clearInterval(handler)
            }
          }, 50)
        })
      }
      if (!window.StripeCheckout) {
        return this.$notify.error({
          title: 'Loading failed',
          message: 'Stripe checkout could not be loaded',
        })
      }
      window.StripeCheckout.configure(options).open(opts)
    },
  }
}


/** SAMPLE **
{
  "id": "tok_1DSNtuBd1vTrDtfMGkjjSa5C",
  "object": "token",
  "client_ip": "103.58.74.174",
  "created": 1541246810,
  "email": "admin@infancyit.com",
  "livemode": false,
  "type": "card",
  "used": false,
  "card": {
    "id": "card_1DSNtuBd1vTrDtfMNBT3FTL6",
    "object": "card",
    "address_city": null,
    "address_country": null,
    "address_line1": null,
    "address_line1_check": null,
    "address_line2": null,
    "address_state": null,
    "address_zip": null,
    "address_zip_check": null,
    "brand": "Visa",
    "country": "US",
    "cvc_check": "pass",
    "dynamic_last4": null,
    "exp_month": 6,
    "exp_year": 2022,
    "funding": "credit",
    "last4": "4242",
    "metadata": {},
    "name": "admin@infancyit.com",
    "tokenization_method": null
  }
}
*/
