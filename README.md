# Ecomap Frontend Applications
[![Netlify Status](https://api.netlify.com/api/v1/badges/167e2f21-a456-4e07-96b0-53e35df63f4a/deploy-status)](https://app.netlify.com/sites/ecomap/deploys)


## Technical Specifications
- Nuxt.js project (SPA Mode)
- Based on Vue 2
- VueX for Store Management


## Development Method
If you are working on this project **NEVER COMMIT TO MASTER BRANCH DIRECTLY** 
1. Clone the Project in your local computer "git clone git@gitlab.com:w3dev-clients/ecomap/_v2/web-app.git"
2. Follow the Build Setup (see below)
3. Create a new branch in the format `feature/FEATURE_NAME`
4. Make changes in that branch
5. Push the changes to feature branch after changes are done
6. Create a Pull Request with master branch after your changes are done
7. You can visit [this URL](https://gitlab.com/w3dev-clients/ecomap/web-app/-/merge_requests/new) to create a new PR

## Build Setup (Local Deployment)

``` bash
# install dependencies
$ npm install # Or yarn install

# create .env file
$ cp .env.example .env

# serve with hot reload at localhost:8080
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## Deploy to Server (Production)

1. Netlify or Any Static Hosting Website Could be used which support environment variables.
2. On Netlify, Create a New Project
3. Select the Repository to this one.
4. In Build Command, enter `npm run build && mv dist/200.html dist/index.html && cp _redirects dist/_redirects`
5. Select Publish Directory as `dist`
6. Deploy the Site
7. Go to [Netlify Site Environment](https://app.netlify.com/sites/ecomap/settings/deploys#environment)
8. Setup Environment Variables as per `env.example`


## Default .env

### Api Settings
'API_URL_V3=https://ecomap.w3api.net/v1'

'API_URL=https://ecomap.w3api.net/v1'

'API_URL_BROWSER=https://ecomap.w3api.net/v1'

### Stripe Details
'STRIPE_KEY=pk_live_QGbh8cUbQnimhiS8dBvgoAVR'

'STRIPE_SECRET=sk_live_ajea2yNKkTkpGseqdwUWDKzY'

'STRIPE_PRODUCT_ID=prod_HzD0TpcnqEcgSf'
