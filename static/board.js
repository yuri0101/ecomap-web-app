window['ECOMAP_BOARD'] = {}

let stage, stageHistory, layer, shapeLayer, linkLayer, tooltipLayer, tooltip, tr, selectionRectangle;
stageHistory = [];

let allShapeGroups = []

let textTr;
let tempShape = null;

let movingShape = null
let targetMovingModule;

let tempScaleElement = null
let tempScaleVal = {x:1,y:1}

let lastShapeCount = 1;

let stageScale = {
  x:1,
  y:1
}

const customDebounce = (func, delay) => {
  let debounceTimer
  return function() {
      const context = this
      const args = arguments
      clearTimeout(debounceTimer)
      debounceTimer = setTimeout(() => func.apply(context, args), delay)
  }
}

const setScaleToOne = (e) => {
  // alert("Ho")
  e.height(e.height()*tempScaleVal.y)
  e.width(e.width()*tempScaleVal.x)

  tempScaleVal = {x:1, y: 1}
  e.scale(tempScaleVal)

  let ai = allShapeGroups.findIndex(x => x.name == e?.attrs?.name)
  allShapeGroups[ai].ClientRect = e.getClientRect()

  // console.log("====REDRAW=====")

  shapeLayer.draw()
}

function getParentGroup(e){
  let g = e;
  //// console.log("G",g);
  if(e && e.attrs && e.attrs.targetGroup){
    return getParentGroup(stage.find(e.attrs.targetGroup)[0])
  }
  else{
    targetMovingModule = g
    targetMovingModule.draggable(false)
    return targetMovingModule
  }
}
function drawthings(animLayer, connections){
    for(let i = 0;i<connections.length;i++){
        animLayer.add(connections[i]);
        animLayer.batchDraw();
    }
}
function stage_onWheel(e){
  e.evt.preventDefault();
  var scaleBy = 1.1;
  var oldScale = stage.scaleX();

  var pointer = stage.getPointerPosition();

  if(!window.ECOMAP_BOARD.stageDefaultPosition)
    window.ECOMAP_BOARD.stageDefaultPosition = {
      x: stage.x(),
      y: stage.y()
    }

  var mousePointTo = {
    x: (pointer.x - stage.x()) / oldScale,
    y: (pointer.y - stage.y()) / oldScale,
  };

  var newScale =
    e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

  stage.scale({ x: newScale, y: newScale });
  stageScale.x = newScale
  stageScale.y = newScale

  var newPos = {
    x: pointer.x - mousePointTo.x * newScale,
    y: pointer.y - mousePointTo.y * newScale,
  };
  stage.position(newPos);
  stage.batchDraw();
}

let drawingLine = false;
let source = null;
let line;
let x1, y1, x2, y2;

function stage_onMouseDown(e){

  // console.log("MOUSE_DOWN", e.target)
  movingShape = e.target

  var transform = shapeLayer.getAbsoluteTransform().copy();
  transform.invert();

  let transformedPointer =  transform.point(stage.getPointerPosition());
  let stageX = transformedPointer.x;
  let stageY = transformedPointer.y;

  // console.log(stageX);
  // console.log(stageY);

  if(window.SELECTION_MODE_ON){
    // do nothing if we mousedown on any shape16
    // if (e.target !== stage) {
    //   return;
    // }
    x1 = stageX;
    y1 = stageY;
    x2 = stageX;
    y2 = stageY;

    selectionRectangle.visible(true);
    selectionRectangle.width(0);
    selectionRectangle.height(0);

  }

  //draw rectangular marker
  if(window.DRAW_SHAPE_RECTANGLE){
    x1 = stageX;
    y1 = stageY;
    // console.log("Drawing Shape")
    tempShape= new Konva.Rect({
      x: stageX,
      y: stageY,
      name:'temp_shape',
      fill: "#F0FCE5",
      stroke: "#82C341",
      strokeWidth: 1,
      opacity: 0.9,
      cornerRadius: 3,
      draggable: true
    });

    tempShape.on('dragend', e => {
      //// console.log("Dragging:", e)
      let ai = allShapeGroups.findIndex(x => x.name == e.target?.attrs?.name)
      allShapeGroups[ai].ClientRect = e.target.getClientRect()
    })

    tempShape.on('scaleXChange', e => {
      //console.clear()
      let nScaleX = e.newVal
      tempScaleVal.x = nScaleX
      //// console.log("Scaling:", e)
      customDebounce(()=>{
        setScaleToOne(e.currentTarget)
      },100)()
    })
    tempShape.on('scaleYChange', e => {
      let nScaleY = e.newVal
      tempScaleVal.y = nScaleY
      // // console.log("Scaling:", e)
      customDebounce(()=>{
        setScaleToOne(e.currentTarget)
      },100)()
    })

    shapeLayer.add(tempShape)
    shapeLayer.draw()
  }


  // Drawing Line
  if(e.target?.attrs?.name == 'connector'){
      //// console.log("MOUSE DOWN On CONNECTOR")
      source = getParentGroup(e.target)

      // console.log('Moving Source:', source)
      drawingLine = true;

      window['DRAG_START_EL_ID'] = '#' + source.id()
      //// console.log("Source ID:", window['DRAG_START_EL_ID'])

      const pos = stage.getPointerPosition();
      const dx = pos.x - source.x()
      const dy = pos.y - source.y()
      let angle = Math.atan2(-dy, dx);

      points = [
        source.x() + 100,
        source.y() + 100,
        pos.x ,
        pos.y ,
      ]

      line = new Konva.Line({
          stroke: 'black',
          // bezier: true,
          // remove line from hit graph, so we can check intersections
          listening: false,
          points
      });

      layer.add(line);
  }

  layer.draw();
}
function stage_onMouseOver(e){
  // Drawing Line
  if (e.target.hasName('connector')) {
    e.target.stroke('black');
    layer.draw();
  }
  if(e.target.hasName('stakeholder-circle')){
    var mousePos = e.target.getStage().getPointerPosition();
    tooltip.position({
      x: mousePos.x,
      y: mousePos.y
    });
    tooltip.getText().text(e.target?.attrs?.title)
    tooltip.hide()
    tooltipLayer.batchDraw()
  }
}
function stage_onMouseOut(e){
  // Drawing Line
  if (e.target.hasName('connector')) {
    e.target.stroke(null);
    layer.draw();
  }
  if(!e.target.hasName('stakeholder-circle')){
    tooltip.hide()
    tooltipLayer.batchDraw()
  }
}
function stage_onMouseMove(e){

  let stagePos = stage.getPointerPosition()

  if(window.DRAW_SHAPE_RECTANGLE){
    document.body.style.cursor = 'crosshair';
    if(tempShape){
      // // console.log(tempShape)
      tempShape.setAttrs({
        height: Math.abs(stagePos.y - y1),
        width: Math.abs(stagePos.x - x1)
      })
      shapeLayer.batchDraw()
    }
    return;
  }else{
    document.body.style.cursor = 'pointer';
  }

  if(window.SELECTION_MODE_ON){
    // no nothing if we didn't start selection
    if (!selectionRectangle.visible()) {
      return;
    }

    x2 = stage.getPointerPosition().x;
    y2 = stage.getPointerPosition().y;

    selectionRectangle.setAttrs({
      x: Math.min(x1, x2),
      y: Math.min(y1, y2),
      width: Math.abs(x2 - x1),
      height: Math.abs(y2 - y1),
    });
  }


  //// console.log("Moving:", e.target)

  // Drawing Line
  if (!line) {
      layer.batchDraw();
      return;
  }

  const pos = stage.getPointerPosition();
  const points = line.points().slice();
  points[2] = pos.x;
  points[3] = pos.y;
  //  // console.log("POINTS", points)

  line.points(points);

  layer.batchDraw();

}
function stage_onMouseUp(e){

    // console.log("ATTRS:", movingShape?.attrs)
    // Check if in a group
    if(allShapeGroups.length > 0 && movingShape?.attrs?.targetGroup){
      // let AllShapes = stage.find('.moduleCircleGroup').toArray();
      for(let i=0; i < allShapeGroups.length; i++){

        let magneticPadding = 100
        // let extendedClientRect = allShapeGroups[i].ClientRect
        // extendedClientRect.height += magneticPadding*2
        // extendedClientRect.width += magneticPadding*2
        // extendedClientRect.x -= magneticPadding
        // extendedClientRect.y -= magneticPadding
        // if(Konva.Util.haveIntersection(extendedClientRect, movingShape.getClientRect())){
        //   if(Konva.Util.haveIntersection(allShapeGroups[i].ClientRect, movingShape.getClientRect())){
        //     // console.log("IN_GROUP:", allShapeGroups[i].name)
        //   }else{
        //     // console.log("BORDER_GROUP:",allShapeGroups[i].name)
        //   }
        // }

        let targetGroupId = movingShape?.attrs?.targetGroup.replace("#","")
        let moduleInGroup = allShapeGroups[i].modules.filter(e=>e == targetGroupId)
        if(moduleInGroup.length == 0){
          allShapeGroups[i].modules.push(targetGroupId)
        }

        if(Konva.Util.haveIntersection(allShapeGroups[i].ClientRect, movingShape.getClientRect())){
          // console.log("IN_GROUP:", allShapeGroups[i].name, allShapeGroups[i].ClientRect)
          // let selectedGrpPosition = allShapeGroups[i].ClientRect;
          // let groupCoords = [
          //   [selectedGrpPosition.x, selectedGrpPosition.y],
          //   [selectedGrpPosition.x+selectedGrpPosition.width, selectedGrpPosition.y],
          //   [selectedGrpPosition.x, selectedGrpPosition.y+selectedGrpPosition.height],
          //   [selectedGrpPosition.x+selectedGrpPosition.width, selectedGrpPosition.y+selectedGrpPosition.height]
          // ]
          // let movingModuleRect = movingShape.getClientRect();
          // let movingModuleCoords = [
          //   movingModuleRect.x+movingModuleRect.width/2,
          //   movingModuleRect.y+movingModuleRect.height/2
          // ]
          // // console.log("COORDS", movingModuleCoords, groupCoords)
          let movingModuleRect = movingShape.getClientRect();
          let movingModuleCoords = [
            [
              movingModuleRect.x,
              movingModuleRect.y,
            ], // Top Left
            [
              movingModuleRect.x,
              movingModuleRect.y+movingModuleRect.height
            ], // Bottom Left
            [
              movingModuleRect.x+movingModuleRect.width,
              movingModuleRect.y
            ], // Top Right
            [
              movingModuleRect.x+movingModuleRect.width,
              movingModuleRect.y+movingModuleRect.height,
            ], // Bottom Right
          ]
          // .map(coords => {
          //   return [coords[0]*stageScale.x, coords[1]*stageScale.y]
          // })

          let InsideShape = [0,0,0,0]

          let shapeClientRect = allShapeGroups[i].ClientRect

          for(let j=0; j < 4; j++){
            if(Konva.Util.haveIntersection(shapeClientRect, {x: movingModuleCoords[j][0], y:movingModuleCoords[j][1], height:0, width:0})){
              InsideShape[j] = 1;
            }else{
              InsideShape[j] = 0
            }
          }

          // console.log("Inside Shape:", InsideShape, shapeClientRect, movingModuleCoords)

          // Parsing Corner Extension
          if(InsideShape.filter(e => e === 1).length == 1){

            let extraPadding = 20;

            // Bottom Right Extension
            if(InsideShape[0] == 1){

              let targetXY = movingModuleCoords[3].map(e=>e+extraPadding)
              let targetShape = stage.find(`.${allShapeGroups[i].name}`)[0]

              let targetShapeRect = targetShape.getClientRect()
              //console.clear();
              // console.log("Old Target Rect: ", targetShapeRect)
              let newClientRect = {
                x:targetShapeRect.x,
                y:targetShapeRect.y,
                width: (targetXY[0] - targetShapeRect.x),
                height: (targetXY[1] - targetShapeRect.y)
              }
              // console.log("New Target Rect: ", newClientRect)
              allShapeGroups[i].ClientRect = newClientRect
              targetShape.width(newClientRect.width)
              targetShape.height(newClientRect.height)

            }

            /**
             * To Do
             * Magnetic Extension: Top Left, Top Right, Bottom Left
             */

          }

        }

      }
      shapeLayer.draw()
    }

    movingShape = null

    if(targetMovingModule){
      targetMovingModule.draggable(true)
    }

    if(window.DRAW_SHAPE_RECTANGLE){
      // window.DRAW_SHAPE_RECTANGLE = false;
      tempShape.setAttrs({
        name: `shape${lastShapeCount}`
      })

      var shapes = stage.find('.moduleCircleGroup').toArray();
      var box = tempShape.getClientRect();
      // console.log("BoxCoords", box)
      var selected = shapes.filter((shape) =>
        Konva.Util.haveIntersection(box, shape.getClientRect())
      );

      allShapeGroups.push({
        name: `shape${lastShapeCount}`,
        modules: selected.map(e=>{
          return e.attrs?.mId
        }),
        ClientRect: box,
      })

      lastShapeCount++;
      // console.log("Stakeholders in Group:", selected)

      //set a title of the group
      if(confirm("Do you want to add a group title?")){
        let title = prompt("Enter Group Title")
        if(title && title.length > 0){
          let groupTitle = new Konva.Text({
            text: title,
            x: tempShape.x() + 20,
            y: tempShape.y() + 20,
            fontFamily: 'Inter',
            fontSize: 20,
            padding: 5,
            fill: '#82C341',
            draggable:true
          })
          shapeLayer.add(groupTitle)
        }
      }
      shapeLayer.batchDraw()
      tempShape = null;

      // Unselect Toolbar
      window['DRAW_SHAPE_RECTANGLE'] = false;
      document.querySelector(".toolbar_shape").classList.remove("active")

      // Unselect Resize Bar
      window.SELECTION_MODE_ON = false

      return;
    }

    if(window.SELECTION_MODE_ON){
      // no nothing if we didn't start selection
      if (!selectionRectangle.visible()) {
        return;
      }
      // update visibility in timeout, so we can check it in click event
      setTimeout(() => {
        selectionRectangle.visible(false);
        layer.batchDraw();
      });

      var shapes = stage.find('.moduleCircleGroup').toArray();
      //("All Shapes:", shapes)

      var box = selectionRectangle.getClientRect();
      var selected = shapes.filter((shape) =>
        Konva.Util.haveIntersection(box, shape.getClientRect())
      );
      // console.log("Selection:", selected)
      tr.nodes(selected);
      layer.batchDraw();
    }


    // Click on Blank Canvas
    if(e?.target?.attrs?.name == "ecomap-canvas"){

      // Remove Transformer of Text
      if(textTr) textTr.hide()

    }

    // Selecting a Group Shape
    // alert(e.target?.attrs?.name?.substring(0,5))
    if(e.target?.attrs?.name?.substring(0,5) == "shape"){
      setTimeout(() => {
        selectionRectangle.visible(false);
        layer.batchDraw();
      });
      // console.log("SELECTING:", e.target)
      tr.nodes([e.target])
      layer.batchDraw()
    }else{
      // console.log("TARGETL:", e.target)
    }

    // Draw Line
    if (!line) {
        return;
    }

    // Selecting a Stakeholder
    if(e.target?.attrs?.targetGroup){

      let source = stage.find(`${e.target.attrs.targetGroup}`)[0]
      let dest = stage.find(window.DRAG_START_EL_ID)[0]

      var arrow = new Konva.Arrow({
          id:`${source.id()}-${dest.id()}`,
          source: source,
          dest: dest,
          name:'arrow-connector',
          pointerLength: 10,
          pointerWidth: 10,
          fill: 'rgba(0,0,0,0.7)',
          stroke: 'rgba(0,0,0,0.7)',
          strokeWidth: 2
      });

      function adjustPoint(e){

        let sourceCircleId = source.id().replace("mainGroup","circle");
        let destCircleId = dest.id().replace("mainGroup","circle")
        let sourceCircle = stage.find(`#${sourceCircleId}`)[0]
        let destCircle = stage.find(`#${destCircleId}`)[0]

        const sourceRadius = sourceCircle.radius()*sourceCircle.parent.attrs.scaleX;
        const destRadius = destCircle.radius()*destCircle.parent.attrs.scaleY;

        const dx = sourceCircle.absolutePosition().x - destCircle.absolutePosition().x;
        const dy = sourceCircle.absolutePosition().y - destCircle.absolutePosition().y;
        let angle = Math.atan2(-dy, dx);

        var p=[
          destCircle.absolutePosition().x
          + -destRadius * Math.cos(angle + Math.PI)
          ,
          destCircle.absolutePosition().y
          + destRadius * Math.sin(angle + Math.PI)
          ,
          sourceCircle.absolutePosition().x
          + -sourceRadius * Math.cos(angle)
          ,
          sourceCircle.absolutePosition().y
          + sourceRadius * Math.sin(angle)
          ,
        ];
        arrow.setPoints(p);
        linkLayer.draw()
        layer.draw();
      }

      adjustPoint()

      source.on('dragmove', adjustPoint)
      dest.on('dragmove', adjustPoint)

      line.destroy();
      linkLayer.add(arrow);
      linkLayer.draw()
      line = null;

    } else{
      line.destroy()
      layer.draw()
      line = null
    }

    // if (!e.target.hasName('connector')) {
    //     line.destroy();
    //     layer.draw();
    //     line = null;
    // } else {
    //     line = null;
    // }

    stageHistory.push(stage.toJSON())


}
function stage_onClick(e){

  //// console.log("CLICK_TAP")
  // if we are selecting with rect, do nothing
  // if (selectionRectangle.visible()) {
  //   return;
  // }

  // console.log("CLICK_EVT", e.target)

  // if click on empty area - remove all selections
  if (e.target === stage) {
    tr.nodes([]);
    layer.draw();
    return;
  }

  // do nothing if clicked NOT on our rectangles
  // if (!e.target.hasName('rect')) {
  //   return;
  // }

  // do we pressed shift or ctrl?
  const metaPressed = e.evt.shiftKey || e.evt.ctrlKey || e.evt.metaKey;
  const isSelected = tr.nodes().indexOf(e.target) >= 0;

  if (!metaPressed && !isSelected) {
    // if no key pressed and the node is not selected
    // select just one
    if(e.target && e.target.attrs.targetGroup){
      tr.nodes([stage.find(e.target.attrs.targetGroup)[0]])
    }
    // console.log("SELECTED:", e.target)
    //tr.nodes([e.target]);
  } else if (metaPressed && isSelected) {
    // if we pressed keys and node was selected
    // we need to remove it from selection:
    const nodes = tr.nodes().slice(); // use slice to have new copy of array
    // remove node from array
    nodes.splice(nodes.indexOf(e.target), 1);
    tr.nodes(nodes);
  } else if (metaPressed && !isSelected) {
    // add the node into selection
    const nodes = tr.nodes().concat([e.target]);
    tr.nodes(nodes);
  }
  layer.draw();

  if(window['ECOMAP_BOARD'].onElementClick){

    if(e.target && e.target.attrs && e.target.attrs.id && e.target.attrs.targetGroup){      let elId = e.target.attrs.id.replace("option","")
      let elCircle = stage.find(e.target.attrs.targetGroup)[0]


      //mini-pop-up contents
      window['ECOMAP_BOARD'].onElementClick({
        id: elId,
        title: elCircle.attrs.title,
        details: elCircle.attrs.details,
        website: elCircle.attrs.website,
        imageUrl: elCircle.attrs.imageUrl
      }, e.evt)

    }
  }
}
function startResize(e){
    // let tEl = e.target
    // if(e.target.attrs && e.target.attrs.tags == "connectors") return false;
    // if(e.target.attrs && e.target.attrs.targetGroup) tEl = stage.find(e.target.attrs.targetGroup)[0]
    // return tr.nodes([tEl])
}
function handleDragEnd(e) {
    // this/e.target is the source node.
    // [].forEach.call(images, function (img) {
    //   img.classList.remove("img_dragging");
    // });
}
function handleExternalDrop(e){
    if (e.stopPropagation) {
        e.stopPropagation(); // stops the browser from redirecting.
    }
}
function handleDragStart(e) {

  //// console.log("Drag Start:", e.target)

  // Firefox Fix
  // e.dataTransfer.setData("text/plain",  e.target);
  // e.dataTransfer.effectAllowed = "copy";

//     [].forEach.call(images, function (img) {
//       img.classList.remove("img_dragging");
//     });
//     this.classList.add("img_dragging");
}

function handleTouchStart(e) {
    // [].forEach.call(images, function (img) {
    //   img.classList.remove("img_dragging");
    // });
    // this.classList.add("img_dragging");
}

function handleDragOver(e) {
    if (e.preventDefault) {
      e.preventDefault(); // Necessary. Allows us to drop.
    }

    e.dataTransfer.dropEffect = "copy";
    return false;
}

function handleDragEnter(e) {
    //// console.log("Drag Start:", e, e.relatedTarget.nodeName )
    window['DRAG_ELEMENT'] = e.relatedTarget.nodeName == "IMG" ? e.relatedTarget:e.relatedTarget.querySelector('img');
    //// console.log("Type:", window.DRAG_ELEMENT.nodeName)
    // this / e.target is the current hover target.
    this.classList.add("over");
  }

function handleDragLeave(e) {
    this.classList.remove("over"); // this / e.target is previous target element.
}

function swipeIt(e) {
    var contact = e.touches;
    this.style.left = initX + contact[0].pageX - firstX + "px";
    this.style.top = initY + contact[0].pageY - firstY + "px";
}


function setupMenu(){

    let currentShape;
    var menuNode = document.getElementById('menu');

    document.getElementById('pulse-button').addEventListener('click', () => {
      currentShape.to({
        scaleX: 2,
        scaleY: 2,
        onFinish: () => {
          currentShape.to({ scaleX: 1, scaleY: 1 });
        },
      });
    });

    document.getElementById('delete-button').addEventListener('click', () => {
        //// console.log("Delete", currentShape.attrs.targetGroup)
        if(currentShape.attrs && currentShape.attrs.targetGroup){
          stage.find(currentShape.attrs.targetGroup)[0].destroy()
        }
        const tr = layer.find('Transformer').toArray().find(tr => tr.nodes()[0] === currentShape);
        const shapetr = shapeLayer.find('Transformer').toArray().find(tr => tr.nodes()[0] === currentShape);
        if (tr) tr.destroy();
        if (shapetr) shapetr.destroy();

        currentShape.destroy();
        layer.draw();
        shapeLayer.draw();

        // currentShape.destroy();
        // layer.draw();

    });

    window.addEventListener('click', () => {
        // hide menu
        menuNode.style.display = 'none';
    });

    stage.on('contextmenu', function (e) {
        // prevent default behavior
        e.evt.preventDefault();
        if (e.target === stage) {
          // if we are on empty place of the stage we will do nothing
          return;
        }
        currentShape = e.target;
        // show menu
        menuNode.style.display = 'initial';
        menuNode.style.zIndex = 5;
        menuNode.style.position = 'absolute'
        var containerRect = stage.container().getBoundingClientRect();
        menuNode.style.top =
          containerRect.top + stage.getPointerPosition().y + 4 + 'px';
        menuNode.style.left =
          containerRect.left + stage.getPointerPosition().x + 4 + 'px';
    });

    stage.on('click', function(e){
        return true;
        //// console.log('clicked on', e.target);
        // console.log(
        //   'usual click on ' + JSON.stringify(stage.getPointerPosition())
        // );
        //// console.log(e)
        var infoNode = document.querySelector("#info-popup");
        var containerRect = stage.container().getBoundingClientRect();
        infoNode.style.display = 'initial';
        infoNode.style.top = containerRect.top + stage.getPointerPosition().y + 4 + 'px';
        infoNode.style.left = containerRect.left + stage.getPointerPosition().x + 4 + 'px';
    })


}

function loadImages(sources, callback) {
    var images = {};
    var loadedImages = 0;
    var numImages = 0;
    // get num of sources
    for (var src in sources) {
      numImages++;
    }
    for (var src in sources) {
      images[src] = new Image();
      images[src].onload = function () {
        if (++loadedImages >= numImages) {
          callback(images);
        }
      };
      images[src].src = sources[src];
    }
}
function loadImage(HTMLImg){
  return new Promise(resolve => {

    setTimeout(()=>{
      // console.log("Image Loading Failed:", HTMLImg.src)
      return resolve(`FAILED:${HTMLImg.src}`)
    },5000)

    HTMLImg.onload = () => {
      // console.log("ImageLoaded:", HTMLImg.src)
      return resolve('IMAGE_LOADED')
    }
  })
}
function draw(existingData = null) {
    var width = window?.screen?.width || window.innerWidth;
    var height = window?.screen?.height || window.innerHeight;

    if(existingData){

      let i_layer = 0;
      stage = Konva.Node.create(existingData, 'ecomap-board')

      if(stage.children[0] && stage.children[0].attrs && stage.children[0].attrs.name){
        shapeLayer =  stage.children[0]
        i_layer = 1
      }

      linkLayer = stage.children[i_layer]
      layer = stage.children[i_layer+1]
      tooltipLayer = stage.children[i_layer+2]

      let allCircles = stage.find(".stakeholder-circle")
      let imgEls = []
      let loadImagePromise = []
      for(let i=0; i<allCircles.length; i++){
        let x = allCircles[i]
        let imgSrc = x.attrs.imageUrl;
        // console.log("image:", imgSrc)
        let imgEl = document.createElement("img")
        imgEl.src = imgSrc;
        imgEls.push(imgEl)

        loadImagePromise.push(loadImage(imgEl))
      }

      Promise.all(loadImagePromise).then(d=>{

        setTimeout(()=>{
          for(let i=0; i<allCircles.length; i++){
            allCircles[i].fillPatternImage(imgEls[i])
          }
          // console.log("IMAGE_LOAD:", d)
          layer.batchDraw()

          let allGroups = stage.find(".moduleCircleGroup")
          for(let i=0; i < allGroups.length; i++){

            allGroups[i].on('mouseover',(e)=>{
              allGroups[i].children[1].visible(true)
            })

            allGroups[i].on('mouseout',(e)=>{
              allGroups[i].children[1].visible(false)
            })
          }

          let allArrowConnectors = stage.find(".arrow-connector");
          function adjustPoints(arrow, source, dest){

            let sourceCircleId = source.id().replace("mainGroup","circle");
            let destCircleId = dest.id().replace("mainGroup","circle")

            let sourceCircle = stage.find(`#${sourceCircleId}`)[0]
            let destCircle = stage.find(`#${destCircleId}`)[0]

            const sourceRadius = sourceCircle.radius()*sourceCircle.parent.attrs.scaleX;
            const destRadius = destCircle.radius()*destCircle.parent.attrs.scaleY;

            const dx = sourceCircle.absolutePosition().x - destCircle.absolutePosition().x;
            const dy = sourceCircle.absolutePosition().y - destCircle.absolutePosition().y;

            let angle = Math.atan2(-dy, dx);

            var p=[
              destCircle.absolutePosition().x
              + -destRadius * Math.cos(angle + Math.PI)
              ,
              destCircle.absolutePosition().y
              + destRadius * Math.sin(angle + Math.PI)
              ,
              sourceCircle.absolutePosition().x
              + -sourceRadius * Math.cos(angle)
              ,
              sourceCircle.absolutePosition().y
              + sourceRadius * Math.sin(angle)
              ,
            ];

            arrow.setPoints(p);
            linkLayer.draw()
            layer.draw();
          }
          for(let i=0; i < allArrowConnectors.length; i++){
            let a = allArrowConnectors[i]
            const [sourceId, destId] = a.attrs.id.split("-")
            let source = stage.findOne(`#${sourceId}`)
            let dest = stage.findOne(`#${destId}`)
            //// console.log(source.id(), dest.id())
            source.on('dragmove',(e)=>{adjustPoints(a, source, dest)})
            dest.on('dragmove',(e)=>{adjustPoints(a, source, dest)})
          }
       },1000)
      })
      .catch(e=>{
        // console.log("ERROR IN LOADING ALL IMAGES: ", e)
      })



    }else{

      stage = new Konva.Stage({
          container: 'ecomap-board',
          name:'ecomap-canvas',
          width: width,
          height: height
      });

      shapeLayer = new Konva.Layer({
        name:'shape-layer'
      })

      layer = new Konva.Layer();
      linkLayer = new Konva.Layer();
      tooltipLayer =  new Konva.Layer()

      tr = new Konva.Transformer();
      tr.nodes([]);
      layer.add(tr);


      tooltip = new Konva.Label({
        opacity: 0.75,
        visible: false,
        listening: false
      });

      tooltip.add(
        new Konva.Tag({
          fill: 'black',
          fontFamily: 'Inter',
          pointerDirection: 'up',
          pointerWidth: 10,
          pointerHeight: 10,
          lineJoin: 'round',
          shadowColor: 'black',
          shadowBlur: 10,
          shadowOffsetX: 10,
          shadowOffsetY: 10,
          shadowOpacity: 0.2
        })
      );

      tooltip.add(
        new Konva.Text({
          text: '',
          fontFamily: 'Intro',
          fontSize: 14,
          padding: 5,
          fill: 'white'
        })
      );
      tooltipLayer.add(tooltip);

    }

    stage.on('wheel', stage_onWheel);

    // add the layer to the stage

    stage.add(shapeLayer);
    stage.add(layer);
    stage.add(linkLayer)
    stage.add(tooltipLayer)

    selectionRectangle = new Konva.Rect({
        fill: 'rgba(0,0,255,0.5)',
    });
    layer.add(selectionRectangle);

    // stage.on('mousedown touchstart', stage_onMouseDown);
    // stage.on('mouseover', stage_onMouseOver);
    // stage.on('mouseout', stage_onMouseOut);
    // stage.on('mousemove touchmove', stage_onMouseMove);
    // stage.on('mouseup touchend', stage_onMouseUp);

    // clicks should select/deselect shapes
    stage.on('click tap', stage_onClick);

    setupMenu();

    layer.on('mouseover', (e)=>{
        document.body.style.cursor = 'pointer';
        if(e.target && e.target.attrs && e.target.attrs.name == "stakeholder-circle" && e.target.attrs.moduleId){
            stage.find(`#option${e.target.attrs.moduleId}`)[0].visible(true)
        }
        layer.draw();
    })
}

var sources = {
    darthVader: 'https://img.freepik.com/free-vector/bitcoin-golden-logo_48369-12177.jpg?size=338&ext=jpg',
};

//reset zoom tool
function toolbar_resetZoom(){
  if(window.ECOMAP_BOARD.stageDefaultPosition){
    stage.scale({ x: 1, y: 1 });
    stage.position(window.ECOMAP_BOARD.stageDefaultPosition)
    stage.batchDraw()
  }else{
    // console.log("Board not Resized")
  }
}

//hand to move board tool
function toolbar__moveMap(){
  if(document.querySelector(".toolbar_move_map").classList.contains("active")){
    window['STAGE_DRAGGABLE'] = false;
    document.body.style.cursor = "auto"
    stage.draggable(false)
    document.querySelector(".toolbar_move_map").classList.remove("active")
  }else{
    window['STAGE_DRAGGABLE'] = true;
    document.body.style.cursor = "move"
    stage.draggable(true)
    document.querySelector(".toolbar_move_map").classList.add("active")
  }
}

//shape draw tool
function toolbar_shapeDraw(){
  if(document.querySelector(".toolbar_shape").classList.contains("active")){
    window['DRAW_SHAPE_RECTANGLE'] = false;
    document.querySelector(".toolbar_shape").classList.remove("active")
  }else{
    window['DRAW_SHAPE_RECTANGLE'] = true;
    document.querySelector(".toolbar_shape").classList.add("active")
  }
}
function toolbar_startSelection(){
  if(window['SELECTION_MODE_ON']){
    window['SELECTION_MODE_ON'] = false;
    document.querySelector(".toolbar_selection").classList.remove("active")
  }
  else{
    window['SELECTION_MODE_ON'] = true;
    document.querySelector(".toolbar_selection").classList.add("active")
  }
}
function toolbar__addText(){

  const textNode = new Konva.Text({
    text: 'Add text here',
    fontFamily:'Inter',
    x: 50,
    y: 80,
    fontSize: 18,
    draggable: true,
    width: 200,
  });

  layer.add(textNode);

  textTr = new Konva.Transformer({
    node: textNode,
    enabledAnchors: ['middle-left', 'middle-right'],
    // set minimum width of text
    boundBoxFunc: function (oldBox, newBox) {
      newBox.width = Math.max(40, newBox.width);
      return newBox;
    },
  });

  textTr.hide()

  textNode.on('transform', function () {
    // reset scale, so only with is changing by transformer
    textNode.setAttrs({
      width: textNode.width() * textNode.scaleX(),
      scaleX: 1,
    });
  });

  layer.add(textTr);

  layer.draw();

  // textNode.on('click',()=>{
  //   window['ACTIVE_ELEMENT'] = textTr;
  // })

  textNode.on('dblclick dbltap', () => {
    // hide text node and transformer:
    textNode.hide();
    textTr.hide();
    layer.draw();
    var textPosition = textNode.absolutePosition();
    var areaPosition = {
      x: stage.container().offsetLeft + textPosition.x,
      y: stage.container().offsetTop + textPosition.y,
    };
    var textarea = document.createElement('textarea');
    document.body.appendChild(textarea);
    textarea.value = textNode.text();
    textarea.style.position = 'absolute';
    textarea.style.top = areaPosition.y + 'px';
    textarea.style.left = areaPosition.x + 'px';
    textarea.style.width = textNode.width() - textNode.padding() * 2 + 'px';
    textarea.style.height =
      textNode.height() - textNode.padding() * 2 + 5 + 'px';
    textarea.style.fontSize = textNode.fontSize() + 'px';
    textarea.style.border = 'none';
    textarea.style.padding = '0px';
    textarea.style.margin = '0px';
    textarea.style.overflow = 'hidden';
    textarea.style.background = 'none';
    textarea.style.outline = 'none';
    textarea.style.resize = 'none';
    textarea.style.lineHeight = textNode.lineHeight();
    textarea.style.fontFamily = textNode.fontFamily();
    textarea.style.transformOrigin = 'left top';
    textarea.style.textAlign = textNode.align();
    textarea.style.color = textNode.fill();
    rotation = textNode.rotation();
    var transform = '';
    if (rotation) {
      transform += 'rotateZ(' + rotation + 'deg)';
    }

    var px = 0;

    var isFirefox =
          navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    if (isFirefox) {
      px += 2 + Math.round(textNode.fontSize() / 20);
    }
    transform += 'translateY(-' + px + 'px)';
    textarea.style.transform = transform;
    textarea.style.height = 'auto';
    textarea.style.height = textarea.scrollHeight + 3 + 'px';
    textarea.focus();

    function removeTextarea() {
      textarea.parentNode.removeChild(textarea);
      window.removeEventListener('click', handleOutsideClick);
      textNode.show();
      textTr.show();
      textTr.forceUpdate();
      layer.draw();
    }

    function setTextareaWidth(newWidth) {
      if (!newWidth) {
        // set width for placeholder
        newWidth = textNode.placeholder.length * textNode.fontSize();
      }
      // some extra fixes on different browsers
      var isSafari = /^((?!chrome|android).)*safari/i.test(
        navigator.userAgent
      );
      var isFirefox =
        navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
      if (isSafari || isFirefox) {
        newWidth = Math.ceil(newWidth);
      }

      var isEdge =
        document.documentMode || /Edge/.test(navigator.userAgent);
      if (isEdge) {
        newWidth += 1;
      }
      textarea.style.width = newWidth + 'px';
    }

    textarea.addEventListener('keydown', function (e) {
      // hide on enter
      // but don't hide on shift + enter
      if (e.keyCode === 13 && !e.shiftKey) {
        textNode.text(textarea.value);
        removeTextarea();
      }
      // on esc do not set value back to node
      if (e.keyCode === 27) {
        removeTextarea();
      }
    });

    textarea.addEventListener('keydown', function (e) {
      scale = textNode.getAbsoluteScale().x;
      setTextareaWidth(textNode.width() * scale);
      textarea.style.height = 'auto';
      textarea.scrollHeight + textNode.fontSize() + 'px';
    });

    function handleOutsideClick(e) {
      if (e.target !== textarea) {
        removeTextarea();
        tr.hide();
        tr.forceUpdate();
        textNode.text(textarea.value);
        layer.draw()

      }
    }
    setTimeout(() => {
      window.addEventListener('click', handleOutsideClick);
    });
  })
}
function getBoardData(){
  return stage.toJSON()
}
function addModuleToBoard(e, node=null){

    // console.log("Adding Module:", e, node)
    let groupId, moduleLogo;

    if(window.DRAG_ELEMENT){
      moduleLogo = window.DRAG_ELEMENT
      //// console.log(`Height: ${moduleLogo.naturalHeight}, Width: ${moduleLogo.naturalWidth}`)
      groupId = window.DRAG_ELEMENT.getAttribute('data-id')
    }else{
      let imgObject = document.createElement('img');
      imgObject.src = node.image
      moduleLogo = imgObject
      groupId = node.id
    }

    if(!groupId) return;

    let mainGroupId = `mainGroup${groupId}`

    var group =  new Konva.Group({
        id: mainGroupId,
        title: node?.name || '',
        details: node?.details || '',
        website: node?.s_info?.website || '',
        name:'moduleCircleGroup',
        mId: mainGroupId,
        imageUrl: moduleLogo.src,
        x: e.x-150,
        y: e.y-200+stage.y(),
        draggable: true
    })

    var selectionGroup = new Konva.Group({
        targetGroup:`#${mainGroupId}`,
        x: 50,
        y: 50,
    })

    var selectionBackgroundRectangle = new Konva.Rect({
        id:`option${groupId}`,
        targetGroup:`#${mainGroupId}`,
        stroke: '#fb6e00',
        strokeWidth: 2,
        x: 0,
        y: 0,
    });

    selectionGroup.add(selectionBackgroundRectangle)
    selectionBackgroundRectangle.width(100)
    selectionBackgroundRectangle.height(100)
    selectionBackgroundRectangle.visible(false)

    //connectors on hover
    var connectCircleTop = new Konva.Circle({
        name:'connector',
        tags:'connector',
        targetGroup:`#${mainGroupId}`,
        x:50,
        y: 0,
        radius: 5,
        fill:'#FB6E00',
        opacity:0.75,
    })
    selectionGroup.add(connectCircleTop)

    var connectCircleBottom = new Konva.Circle({
        name:'connector',
        tags:'connector',
        targetGroup:`#${mainGroupId}`,
        x: 50,
        y: 100,
        radius: 5,
        fill:'#FB6E00',
        opacity:0.75,
    })
    selectionGroup.add(connectCircleBottom)

    var connectCircleLeft = new Konva.Circle({
        name:'connector',
        tags:'connector',
        targetGroup:`#${mainGroupId}`,
        x: 0,
        y: 50,
        radius: 5,
        fill:'#FB6E00',
        opacity:0.75,
    })
    selectionGroup.add(connectCircleLeft)

    var connectCircleRight = new Konva.Circle({
        name:'connector',
        tags:'connector',
        targetGroup:`#${mainGroupId}`,
        x: 100,
        y: 50,
        radius: 5,
        fill:'#FB6E00',
        opacity:0.75,
    })
    selectionGroup.add(connectCircleRight)

    //stakeholder logo circle on board
    var NewCircle = new Konva.Circle({
        id:`circle${groupId}`,
        name:'stakeholder-circle',
        title: node?.name || '',
        moduleId: groupId,
        targetGroup:`#${mainGroupId}`,
        x: 100,
        y: 100,
        radius: 40,
        //fill: 'green',
        stroke: '#e5e5e5',
        strokeWidth: 2,
        imageUrl: moduleLogo.src,
        fillPatternScale: {x: 80/moduleLogo.naturalHeight, y:80/moduleLogo.naturalWidth},
        fillPatternOffset:{x:40, y:40},
        fillPatternImage: moduleLogo,
        fillPatternRepeat: 'no-repeat',

    });

    //stakeholder label below logo circle
    var NewCircleLabel = new Konva.Text({
      text: node?.name || '',
      width: 100,
      fontFamily:'Inter',
      fontSize: 12,
      padding: 5,
      x: 50,
      y: 140,
      wrap: true, //'none'
      ellipsis: true,
      fill: 'rgba(0,0,0,0.9)',
      align: 'center'
    })

    group.add(NewCircle)
    group.add(NewCircleLabel)

    group.add(selectionGroup)

    selectionGroup.visible(false)

    group.on('mouseup', startResize)
    group.on('mouseover', (e)=>{
      //// console.log("Mouse Over", e)
      selectionGroup.visible(true)
    })
    group.on('mouseout', ()=>{
      //// console.log("Mouse Out", e)
      selectionGroup.visible(false)
    })

    layer.add(group)
    layer.draw()
    //// console.log("Added to board")
}

function addMarkerToBoard(e, marker=null, id) {

  // console.log("Adding marker:", marker, id)
  var newMarker;
  switch (marker.type) {
    case "rectangle":
      newMarker = new Konva.Rect({
        id: id,
        name: id,
        x: marker.lastClientX,
        y: marker.lastClientY,
        width: marker.width,
        height: marker.height,
        fill: marker.styles.fillColor,
        stroke: "#F1592A",
        strokeWidth: marker.styles.strokeWidth,
        scaleX: marker.scaleX,
        scaleY: marker.scaleY,
        rotation: marker.angle,
        offsetX: 0,
        offsetY: 0,
        draggable: true,
      });
      break;
    case "circle":
      newMarker = new Konva.Circle({
        id: id,
        name: id,
        x: marker.lastClientX,
        y: marker.lastClientY,
        radius: marker.width / 2,
        fill: marker.styles.fillColor,
        stroke: "#F1592A",
        strokeWidth: marker.styles.strokeWidth,
        scaleX: marker.scaleX,
        scaleY: marker.scaleY,
        rotation: marker.angle,
        offsetX: 0,
        offsetY: 0,
        draggable: true,
      })
      break;
    case "textbox":
      newMarker = new Konva.Text({
        id: id,
        name: id,
        x: marker.lastClientX,
        y: marker.lastClientY,
        text: marker.styles.editableText,
        fontSize: 18,
        fontFamily: 'Intro',
        fill: marker.styles.fillColor,
        stroke: "#F1592A",
        strokeWidth: marker.styles.strokeWidth,
        scaleX: marker.scaleX,
        scaleY: marker.scaleY,
        rotation: marker.angle,
        offsetX: 0,
        offsetY: 0,
        draggable: true,
      });
      break;
    case "title":
      newMarker = new Konva.Text({
        id: id,
        name: id,
        x: marker.lastClientX,
        y: marker.lastClientY,
        text: marker.styles.editableText,
        fontSize: 30,
        fontFamily: 'Intro',
        fill: marker.styles.fillColor,
        stroke: "#F1592A",
        strokeWidth: marker.styles.strokeWidth,
        scaleX: marker.scaleX,
        scaleY: marker.scaleY,
        rotation: marker.angle,
        offsetX: 0,
        offsetY: 0,
        draggable: true,
      });
      break;
    case "annular":
      newMarker = new Konva.Arc({
        id: id,
        name: id,
        x: marker.lastClientX,
        y: marker.lastClientY,
        innerRadius: marker.innerRadius,
        outerRadius: marker.outerRadius,
        annularPadding: marker.annularPadding,
        centerPoint: marker.centerPoint,
        // startDegrees: marker.startDegrees,
        // endDegrees: marker.endDegrees,
        angle: marker.endDegrees - marker.startDegrees,
        fill: marker.styles.fillColor,
        stroke: "#F1592A",
        strokeWidth: marker.styles.strokeWidth,
        scaleX: marker.scaleX,
        scaleY: marker.scaleY,
        rotation: marker.startDegrees,
        offsetX: 0,
        offsetY: 0,
        draggable: true,
      });
      break;
    case "line":
      newMarker = new Konva.Line({
        x: marker.lastClientX,
        y: marker.lastClientY,
        points: [marker.lastClientX, marker.lastClientY, marker.lastClientX + 100, marker.lastClientY + 100],
        tension: 1,
        fill: marker.styles.fillColor,
        stroke: "#F1592A",
        strokeWidth: marker.styles.strokeWidth,
        scaleX: marker.scaleX,
        scaleY: marker.scaleY,
        rotation: marker.angle,
        offsetX: 0,
        offsetY: 0,
        draggable: true,
      })
      break;
    case "arrow":
      newMarker = new Konva.Arrow({
        x: marker.lastClientX,
        y: marker.lastClientY,
        points: [0, 0, 200, 0],
        pointerLength: 20,
        pointerWidth: 20,
        fill: marker.styles.fillColor,
        stroke: "#F1592A",
        strokeWidth: marker.styles.strokeWidth,
        scaleX: marker.scaleX,
        scaleY: marker.scaleY,
        rotation: marker.angle,
        offsetX: 0,
        offsetY: 0,
        draggable: true,
      });
      break;
    default:
      break;
  }
  if (["title", "textbox"].includes(marker.type)) {
    
    newMarker.on('dblclick dbltap', () => {
      // hide text node and transformer:
      newMarker.hide();
      tr.hide();
      layer.draw();

      // create textarea over canvas with absolute position
      // first we need to find position for textarea
      // how to find it?

      // at first lets find position of text node relative to the stage:
      var textPosition = newMarker.absolutePosition();

      // so position of textarea will be the sum of positions above:
      var areaPosition = {
        x: stage.container().offsetLeft + textPosition.x,
        y: stage.container().offsetTop + textPosition.y,
      };

      // create textarea and style it
      var textarea = document.createElement('textarea');
      document.body.appendChild(textarea);

      // apply many styles to match text on canvas as close as possible
      // remember that text rendering on canvas and on the textarea can be different
      // and sometimes it is hard to make it 100% the same. But we will try...
      textarea.value = newMarker.text();
      textarea.style.position = 'absolute';
      textarea.style.top = areaPosition.y + 'px';
      textarea.style.left = areaPosition.x + 'px';
      textarea.style.width = newMarker.width() - newMarker.padding() * 2 + 'px';
      textarea.style.height =
        newMarker.height() - newMarker.padding() * 2 + 5 + 'px';
      textarea.style.fontSize = newMarker.fontSize() + 'px';
      textarea.style.border = 'none';
      textarea.style.padding = '0px';
      textarea.style.margin = '0px';
      textarea.style.overflow = 'hidden';
      textarea.style.background = 'none';
      textarea.style.outline = 'none';
      textarea.style.resize = 'none';
      textarea.style.lineHeight = newMarker.lineHeight();
      textarea.style.fontFamily = newMarker.fontFamily();
      textarea.style.transformOrigin = 'left top';
      textarea.style.textAlign = newMarker.align();
      textarea.style.color = newMarker.fill();
      rotation = newMarker.rotation();
      var transform = '';
      if (rotation) {
        transform += 'rotateZ(' + rotation + 'deg)';
      }

      var px = 0;
      // also we need to slightly move textarea on firefox
      // because it jumps a bit
      var isFirefox =
        navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
      if (isFirefox) {
        px += 2 + Math.round(newMarker.fontSize() / 20);
      }
      transform += 'translateY(-' + px + 'px)';

      textarea.style.transform = transform;

      // reset height
      textarea.style.height = 'auto';
      // after browsers resized it we can set actual value
      textarea.style.height = textarea.scrollHeight + 3 + 'px';

      textarea.focus();

      function removeTextarea() {
        textarea.parentNode.removeChild(textarea);
        window.removeEventListener('click', handleOutsideClick);
        newMarker.show();
        tr.show();
        tr.forceUpdate();
        layer.draw();
      }

      function setTextareaWidth(newWidth) {
        if (!newWidth) {
          // set width for placeholder
          newWidth = newMarker.placeholder.length * newMarker.fontSize();
        }
        // some extra fixes on different browsers
        var isSafari = /^((?!chrome|android).)*safari/i.test(
          navigator.userAgent
        );
        var isFirefox =
          navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
        if (isSafari || isFirefox) {
          newWidth = Math.ceil(newWidth);
        }

        var isEdge =
          document.documentMode || /Edge/.test(navigator.userAgent);
        if (isEdge) {
          newWidth += 1;
        }
        textarea.style.width = newWidth + 'px';
      }

      textarea.addEventListener('keydown', function (e) {
        // hide on enter
        // but don't hide on shift + enter
        if (e.keyCode === 13 && !e.shiftKey) {
          newMarker.text(textarea.value);
          removeTextarea();
        }
        // on esc do not set value back to node
        if (e.keyCode === 27) {
          removeTextarea();
        }
      });

      textarea.addEventListener('keydown', function (e) {
        scale = newMarker.getAbsoluteScale().x;
        setTextareaWidth(newMarker.width() * scale);
        textarea.style.height = 'auto';
        textarea.style.height =
          textarea.scrollHeight + newMarker.fontSize() + 'px';
      });

      function handleOutsideClick(e) {
        if (e.target !== textarea) {
          newMarker.text(textarea.value);
          removeTextarea();
        }
      }
      setTimeout(() => {
        window.addEventListener('click', handleOutsideClick);
      });
    });
  }
  if (newMarker) {
    newMarker.on("click", (e) => {

    })
    shapeLayer.add(newMarker)
    var tr = new Konva.Transformer()
    shapeLayer.add(tr)
    tr.nodes([newMarker])
    shapeLayer.draw()
  }
}

function removeMarker(id) {
  var shape = stage.find('#' + id)[0];
  if (shape) shape.remove()
}

function duplicateMarker(marker) {
  // console.log(marker)
}

function handleDrop(e) {
    if(e.preventDefault) e.preventDefault()
    if (e.stopPropagation) e.stopPropagation();

    addModuleToBoard(e)
    return false
}

function initBoard(cb){
  let canvasContainer = document.querySelector("#ecomap-board");
  canvasContainer.addEventListener("dragenter", handleDragEnter, false);
  canvasContainer.addEventListener("dragover", handleDragOver, false);
  canvasContainer.addEventListener("dragleave", handleDragLeave, false);
  canvasContainer.addEventListener("drop", handleDrop, false);

  let contextMenu = document.createElement("div")
  contextMenu.id = "menu"


  let contextMeny_InfoBtn = document.createElement("button")
  contextMeny_InfoBtn.id = "pulse-button"
  contextMeny_InfoBtn.innerHTML = "Info"

  let contextMeny_CustomizeBtn = document.createElement("button")
  contextMeny_CustomizeBtn.id = "customize-button"
  contextMeny_CustomizeBtn.innerHTML = "Customize"

  let contextMeny_DeleteBtn = document.createElement("button")
  contextMeny_DeleteBtn.id = "delete-button"
  contextMeny_DeleteBtn.innerHTML = "Delete"

  let contextMenuContainer = document.createElement("div")
  contextMenuContainer.appendChild(contextMeny_InfoBtn)
  contextMenuContainer.appendChild(contextMeny_CustomizeBtn)
  contextMenuContainer.appendChild(contextMeny_DeleteBtn)

  contextMenu.appendChild(contextMenuContainer)
  canvasContainer.parentNode.insertBefore(contextMenu, canvasContainer.nextSibling)

  let dropElements = document.querySelectorAll(".module-parent img")
  if(dropElements && dropElements.length>0){
    for(let i = 0; i < dropElements.length; i++){
      let el = dropElements[i]

      let initX;
      let initY;
      let firstX;
      let firstY;

      el.addEventListener("dragstart", handleDragStart, false);
      el.addEventListener("dragend", handleDragEnd, false);

      el.addEventListener(
          "touchstart",
          (e) => {
            e.preventDefault();
            initX = this.offsetLeft;
            initY = this.offsetTop;
            var touch = e.touches;
            firstX = touch[0].pageX;
            firstY = touch[0].pageY;

            this.addEventListener("touchmove", swipeIt, false);

            window.addEventListener(
              "touchend",
              function (e) {
                e.preventDefault();
                object.removeEventListener("touchmove", swipeIt, false);
              },
              false
            );
          },
          false
      );
    }
  }

  window.addEventListener('keydown',(e)=>{
    //("Key Pressed:", e.key)
    if(e.key == "Delete"){
      // if(window.ACTIVE_ELEMENT){

      //   // console.log("Active Element:", window.ACTIVE_ELEMENT)
      //   const tr = layer.find('Transformer').toArray().find(tr => {
      //     tr._id === window.ACTIVE_ELEMENT._id
      //     //tr.nodes()[0] === window.ACTIVE_ELEMENT
      //   });
      //   tr.destroy();
      //   window.ACTIVE_ELEMENT.destroy();
      //   layer.draw();

      // }
    }
    if(e.ctrlKey && e.code === 'KeyZ'){
      //// console.log("Undo")
      draw(stageHistory[1])
    }
  })

  cb()
}
loadImages(sources, function (images) {

  window['LOAD_BOARD'] = setInterval(()=>{
    if(document.querySelector("#ecomap-board") && document.querySelectorAll(".module-parent img")){
      initBoard(draw)
      clearInterval(window['LOAD_BOARD'])
    }
  },1000)

});

function drawBoard(existingData){
  window['ECOMAP_BOARD'].boardData = existingData
  window['LOAD_BOARD_DATA'] = setInterval(()=>{
    if(document.querySelector("#ecomap-board") && document.querySelectorAll(".module-parent img")){
      initBoard(()=>{
        draw(existingData)
      })
      clearInterval(window['LOAD_BOARD_DATA'])
    }
  },1000)
}

// window['ECOMAP_BOARD'].draw = drawBoard;
// window['ECOMAP_BOARD'].addModuleToBoard = addModuleToBoard;
// window['ECOMAP_BOARD'].addMarkerToBoard = addMarkerToBoard;
// window['ECOMAP_BOARD'].removeMarker = removeMarker;
// window['ECOMAP_BOARD'].duplicateMarker = duplicateMarker;
// window['ECOMAP_BOARD'].getBoardData = getBoardData;

// window['ECOMAP_BOARD'].addGroupWithStakeholdes = () =>{

// }
