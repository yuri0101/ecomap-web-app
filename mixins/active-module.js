import { mapGetters } from 'vuex'
import sources from '~/assets/data/sources'

export default {
  // Data
  data: () => ({
    instance: null,
  }),
  computed: {
    ...mapGetters({
      logoShape: 'board/logoShape',
      types: 'types/get',
      links: 'board/links',
      groups: 'board/groups',
      modules: 'board/modules',
      nodeDisplaced: 'board/nodeDisplaced',
    }),
    isShared() {
      console.log(this.$route.query.share !== undefined)
      return this.$route.query.share !== undefined
    },
    loggedIn() {
      return this.$store.state.auth.loggedIn
    },
  },

  // Data Change Watchers
  watch: {
    logoShape() {
      if (!this.instance) return
      this.$nextTick(() => {
        this.setAnchor(null, 'inactive', 0)
      })
    },
    modules() {
      if (!this.instance) return
      this.$nextTick(() => {
        this.instance.batch(this.createNodes)
        this.instance.batch(this.createLinks)
      })
    },
    links() {
      if (!this.instance) return
      this.$nextTick(() => {
        this.instance.batch(this.createLinks)
      })
    },
    types(cur, old) {
      if (!this.instance) return
      if (JSON.stringify(cur) === JSON.stringify(old)) return
      this.$nextTick(() => {
        this.instance.deleteEveryEndpoint()
        this.instance.deleteEveryConnection()
        this.instance.batch(this.createNodes)
        this.instance.batch(this.createLinks)
      })
    },
    groups: {
      deep: true,
      handler() {
        this.$nextTick(() => {
          this.buildGroups()
        })
      },
    },
  },

  // Necessary Methods
  methods: {
    initializeBoard() {
    },

    setStyle(node, elem) {
      const pos = node.position || {
        x: 0,
        y: 0,
      }
      const top = pos.y - this.$el.offsetTop + 'px'
      const left = pos.x - this.$el.offsetLeft + 'px'
      elem.css({
        top,
        left,
      })
    },

    buildGroups() {
      Object.keys(this.groups).forEach((key) => {
        if (!document.getElementById(key)) return
        const elem = this.$(`#${key}`)
        const g = Object.assign({}, this.groups[key])
        const top = g.top - this.$el.offsetTop - 20 + 'px'
        const left = g.left - this.$el.offsetLeft - 20 + 'px'
        const width = g.right - g.left + 135 + 'px'
        const height = g.bottom - g.top + 140 + 'px'
        elem.css({
          top,
          left,
          width,
          height,
        })
        const title = elem.find('.group-title')
        const position = Math.floor(title.outerWidth() / 2)
        title.css('left', `calc(50% - ${position}px)`)

        if (!this.isShared) {
          elem.find('.custom-delete-group').on('click', (e) => {
            e.stopPropagation()
            this.$store.dispatch('board/deleteGroupContainer', key)
            $(`#${key}`).remove()
          })
        }
      })
    },

    updatePos(node) {
      const elem = this.$el.querySelector(`#node${node.id}`)
      const position = {
        y: elem.offsetTop + this.$el.offsetTop,
        x: elem.offsetLeft + this.$el.offsetLeft,
      }
      this.$store.dispatch('board/updatePosition', {
        id: node.id,
        position,
      })
      this.$store.dispatch('board/rebuildGroups')
    },

    addConnection(info) {
      const params = {
        source: info.sourceEndpoint.getUuid(),
        target: info.targetEndpoint.getUuid(),
        sourceElem: info.sourceId,
        targetElem: info.targetId,
      }
      if (params.sourceElem === params.targetElem) {
        this.instance.deleteConnection(info.connection)
        return this.$notify.error({
          title: 'Connection Failed',
          message: 'Self loop is not allowed',
        })
      }
      for (let x of this.$store.state.board.links) {
        if (x.source === params.source && x.target === params.target) {
          return
        }
      }
      // if (!this.loggedIn) {
      //   this.instance.deleteConnection(info.connection)
      //   this.$notify.error({
      //     title: 'Unauthorized',
      //     message: 'Please login first to add connection',
      //   })
      //   return this.$store.commit('showLoginModal', true)
      // }
      this.$store.dispatch('board/connect', params)
    },

    removeConnection(labelOverlay) {
      // if (!this.loggedIn) {
      //   this.$notify.error({
      //     title: 'Unauthorized',
      //     message: 'Please login first to remove connection',
      //   })
      //   return this.$store.commit('showLoginModal', true)
      // }
      const linkData = {
        source: labelOverlay.component.endpoints[0].getUuid(),
        target: labelOverlay.component.endpoints[1].getUuid(),
        sourceElem: labelOverlay.component.sourceId,
        targetElem: labelOverlay.component.targetId,
      }
      const link = this.findLink(linkData)
      this.instance.deleteConnection(link)
      this.$store.dispatch('board/disconnect', linkData)
    },

    /* deleteNode(id) {
      this.instance.remove(`node${id}`)
      this.$store.dispatch('board/deleteNode', id)
    }, */

    /* createNodes() {
      // Add modules and endpoints
      for (const node of this.modules) {
        // id of the node
        const ID = `node${node.id}`
        let oldPosition = node.position
        if ($(`.source${node.id}`).length > 0) {
          continue
        }
        // modify attributes
        const elem = this.$('#' + ID)
        this.setStyle(node, elem)
        // set draggable within parent container
        if (!this.isShared) {
          this.instance.draggable(ID, {
            containment: true,
          })
        } else {
          this.instance.draggable(ID, {
            containment: true,
            handle: '.NOHANDLE',
          })
        }
        // add target
        this.addTargets(node)
        // add sources
        this.addSources(node)
        // event listeners
        let timeout
        let $source_node = $(`.source${node.id}`)
        elem.on('mouseup', () => this.updatePos(node))
        elem.on('click', () => {
          const node = this.modules.find(
            (module) => module.id == this.nodeDisplaced,
          )
          if (
            node.position.x === oldPosition.x &&
            node.position.y === oldPosition.y
          ) {
            this.$store.commit('showSmallModal', node)
            this.$store.commit('SmallInBoard', true)
          }
          oldPosition = node.position
        })

        elem.on('mouseenter', () => {
          $source_node.addClass('node-hover')
          elem.addClass('module-hover')
          clearTimeout(timeout)
          timeout = this.setAnchor(`node${node.id}`, 'active', 0)
        })

        elem.on('mouseleave', () => {
          $source_node.removeClass('node-hover')
          elem.removeClass('module-hover')
          clearTimeout(timeout)
          timeout = this.setAnchor(`node${node.id}`, 'inactive', 100)
        })

        $source_node.on('mouseenter', (e) => {
          $source_node.addClass('node-hover')
          elem.addClass('module-hover')
          clearTimeout(timeout)
          timeout = this.setAnchor(`node${node.id}`, 'active', 0)
        })

        $source_node.on('mouseleave', (e) => {
          $source_node.removeClass('node-hover')
          elem.removeClass('module-hover')
          clearTimeout(timeout)
          timeout = this.setAnchor(`node${node.id}`, 'inactive', 100)
        })

        elem.find('.info-btn').on('click', (e) => {
          e.stopPropagation()
        })

        if (!this.isShared) {
          elem.find('.delete-btn').on('click', (e) => {
            e.stopPropagation()
            this.deleteNode(node.id)
          })
        }
      }
    }, */

    addTargets(node) {
      // id of the node
      const ID = `node${node.id}`
      // add target endpoint
      this.instance.addEndpoint(ID, {
        uuid: `${ID}-target`,
        isSource: false,
        isTarget: true,
        maxConnections: -1,
        // endpoint style
        endpoint: [
          'Dot',
          {
            radius: 42,
          },
        ],
        paintStyle: {
          fill: null,
        },
        // anchor style
        anchor: [[0.4, 0.56, 0, 1]],
      })
    },

    getConnectors(name) {
      let connectors = []
      for (const type of this.types) {
        if (type.name === name) {
          connectors = type.connectors || []
          break
        }
      }
      const key = String(connectors.length)
      if (!sources.hasOwnProperty(key)) {
        return sources['1']
      }
      let index = 0
      return connectors.map((label) =>
        Object.assign({}, sources[key][index++], {
          label,
        }),
      )
    },

    addSources(node) {
      // id of the node
      const ID = `node${node.id}`
      let shape = this.logoShape
      shape = shape === 'rectangle' ? 'square' : shape
      // add source endpoints
      const connectors = this.getConnectors(node.type) || []
      for (const source of connectors) {
        const uuid = `${ID}-source-${source.id}`
        let displayClass = 'none'
        let connectorOverlays = [
          [
            'Arrow',
            {
              width: 12,
              length: 12,
              location: 0.8,
            },
          ],
          [
            'Custom',
            {
              location: 0.5,
              create: () =>
                window.$(
                  `<div style="color: ${source.hover};"><span class="label">${source.label}</span></div>`,
                ),
            },
          ],
        ]
        if (!this.isShared) {
          displayClass = 'block'
          connectorOverlays.push([
            'Custom',
            {
              location: 0.5,
              create: () =>
                window.$(
                  '<div><i class="delete icon material-icons">mdi-delete</i></div>',
                ),
              events: {
                click: this.removeConnection,
              },
            },
          ])
        }
        let enabledEndpoint = !this.isShared
        this.instance.addEndpoint(ID, {
          uuid,
          maxConnections: -1,
          isSource: true,
          isTarget: false,
          enabled: enabledEndpoint,
          //- anchor
          anchor: source.anchor.inactive[shape],
          //- endpoint
          endpoint: [
            'Dot',
            {
              radius: 12,
              cssClass: `source source${node.id} source-${source.id} source-${shape} d-${displayClass}`,
              hoverClass: 'source-hover',
            },
          ],
          parameters: {
            anchor: source.anchor,
          },
          paintStyle: {
            fill: source.fill,
          },
          hoverPaintStyle: {
            fill: source.hover,
          },
          //- connector
          connectorStyle: {
            stroke: source.fill,
            strokeWidth: 1.5,
          },
          connectorHoverStyle: {
            stroke: source.hover,
            strokeWidth: 2,
          },
          connectorOverlays: connectorOverlays,
        })
        this.$(`.${uuid}`).attr('title', source.label)
      }
    },

    findLink(linkData) {
      const connections = this.instance.getConnections({
        source: linkData.sourceElem,
        target: linkData.targetElem,
      })
      for (const conn of connections) {
        const id = conn.getUuids()
        if (id && id[0] === linkData.source && id[1] === linkData.target) {
          return conn
        }
      }
    },

    setAnchor(source, type, t) {
      if ($('body').hasClass('jtk-drag-select') && type === 'active') return
      return setTimeout(() => {
        let options
        if (source) {
          options = { source: source }
        }
        let endpoints = this.instance.selectEndpoints(options)
        let shape = this.logoShape
        this.activeAnchor = type === 'active'
        endpoints.each((endpoint) => {
          let params = endpoint.getParameters()
          if (params && params.anchor) {
            endpoint.setAnchor(params.anchor[type][shape])
          }
        })
      }, t)
    },

    createLinks() {
      // Add connections
      for (const data of this.links) {
        // check if connection already exists
        let existing = this.findLink(data)
        // add connection if not exists
        if (existing) continue
        this.instance.connect({
          uuids: [data.source, data.target],
          // connector:[ "Bezier", { curviness: 100 } ],
        })
      }
    },

    // control source visibility on hover
    setOpacity(node, val) {
      const source = this.$(`.source${node.id}`)
      source.css({
        opacity: val || 0,
        'z-index': val ? 15 : 5,
      })
    },
  },
}
