export default {
  data: () => ({
    annularDialog: false,
    draggingMarker: {
      options: null,
      el: null,
      isDragging: false,
    },
    annularOptions: {
      size: 300,
      outerRadius: 150,
      innerRadius: 50,
      padding: 2,
      pieces: 3,
    },
    tabMarkers: [
      {
        options: {
          type: 'rectangle',
          angle: 0,
          selected: true,
          disableScale: true,
          width: 300,
          height: 300,
        },
        icon:
          '<svg width="64px" height="64px" class="svg-logo"> <rect x="1" y="1" width="62" height="62"' +
          'fill="#F69C2A" fill-opacity="0.5" stroke="#F1592A" stroke-width="2" /></svg><span class="name"' +
          '> Rectangle </span>',
      },
      {
        options: {
          type: 'circle',
          angle: 0,
          selected: true,
          disableScale: true,
          width: 300,
          height: 300,
        },
        icon:
          '<svg width="64px" height="64px" class="svg-logo"> <circle cx="32" cy="32" r="31" fill="#F6' +
          '9C2A" fill-opacity="0.5" stroke="#F1592A" stroke-width="2" /> </svg> <span class="name"> Circl' +
          'e </span>',
      },
      {
        options: {
          type: 'textbox',
          angle: 0,
          editableText: 'Type Something',
          selected: true,
          disableScale: true,
          editable: true,
          width: 100,
          height: 50,
        },
        icon:
          '<svg width="64px" height="64px" class="svg-logo"> <rect width="64" height="64" fill="#F69C' +
          '2A" fill-opacity="0.5" stroke="#F1592A" stroke-width="4" /> <text x="24.805" y="45" fill="#F15' +
          '92A" font-size="40" font-weight="bold">t</text> </svg> <span class="name"> Text box </span>',
      },
      {
        options: {
          type: 'title',
          angle: 0,
          editableText: 'Type Something',
          selected: true,
          disableScale: false,
          aspectRatio: true,
          width: 273,
          height: 55,
          editable: true,
        },
        icon:
          '<svg width="64px" height="64px" class="svg-logo"> <rect width="64" height="64" fill="#F69C' +
          '2A" fill-opacity="0.5" stroke="#F1592A" stroke-width="4" /> <text x="19.6" y="47" fill="#F1592' +
          'A" font-size="40" font-weight="bold">T</text> </svg> <span class="name"> Title </span>',
      },
      {
        options: {
          type: 'line',
          angle: 315,
          selected: true,
          disableScale: true,
          width: 300,
          height: 70,
        },
        icon:
          '<svg width="64px" height="64px" class="svg-logo"> <line x1="63" y1="1" x2="1" y2="63" fill' +
          '="#F69C2A" fill-opacity="0.5" stroke="#F1592A" stroke-width="2" /> </svg> <span class="name">L' +
          'ine</span>',
      },
      {
        options: {
          type: 'arrow',
          angle: 315,
          selected: true,
          disableScale: true,
          width: 300,
          height: 70,
        },
        icon:
          '<svg width="64px" height="64px" class="svg-logo"> <polyline points="42,1 63,1 63,20" fill=' +
          '"transparent" fill-opacity="0.5" stroke="#F1592A" stroke-width="2" /> <line x1="63" y1="1" x2=' +
          '"1" y2="63" fill="#F69C2A" fill-opacity="0.5" stroke="#F1592A" stroke-width="2" /> </svg> <spa' +
          'n class="name">Arrow</span>',
      },
      {
        options: {
          type: 'annular',
          angle: 0,
          selected: false,
          disableScale: true,
        },
        icon:
          '<svg width="64px" height="64px" class="svg-logo"> <path d="M 32 1 A 31 31 0 1 0 32 63 A 31' +
          ' 31 0 1 0 31 1 Z M 32 17 A 15 15 0 1 1 32 47 A 15 15 0 1 1 32 17 Z" fill="#F69C2A" fill-opacit' +
          'y="0.5" stroke="#F1592A" stroke-width="2" /> </svg> <span class="name"> Annular </span>',
      },
    ],
  }),
  methods: {
    getBoardPosition() {
      let tm = document.getElementById('ecomap-board')?.style?.transform
      tm = tm.slice('matrix('.length, -1)
      tm = tm.split(',').map(Number)
      return {
        zoom: tm[0],
        tx: tm[4],
        ty: tm[5],
      }
    },
    createAnnularMarker() {
      function optionsWithDefaults(o) {
        // Create a new object so that we don't mutate the original
        var o2 = {
          cx: o.centerX || 0,
          cy: o.centerY || 0,
          startRadians: ((o.startDegrees || 0) * Math.PI) / 180,
          closeRadians: ((o.endDegrees || 0) * Math.PI) / 180,
        }

        var t = o.thickness !== undefined ? o.thickness : 100
        if (o.innerRadius !== undefined) o2.r1 = o.innerRadius
        else if (o.outerRadius !== undefined) o2.r1 = o.outerRadius - t
        else o2.r1 = 200 - t
        if (o.outerRadius !== undefined) o2.r2 = o.outerRadius
        else o2.r2 = o2.r1 + t

        if (o2.r1 < 0) o2.r1 = 0
        if (o2.r2 < 0) o2.r2 = 0

        return o2
      }

      if (Number(this.annularOptions.pieces) < 2) {
        this.annularOptions.pieces = 2
      }
      for (let i = 0; i < Number(this.annularOptions.pieces); i++) {
        var opts = optionsWithDefaults({
          centerX: Number(this.annularOptions.size) / 2,
          centerY: Number(this.annularOptions.size) / 2,
          startDegrees: i * (360 / Number(this.annularOptions.pieces)),
          endDegrees: (i + 1) * (360 / Number(this.annularOptions.pieces)),
          innerRadius: Number(this.annularOptions.innerRadius),
          outerRadius: Number(this.annularOptions.outerRadius),
        })
        var pt = [
          [
            opts.cx + opts.r2 * Math.cos(opts.startRadians),
            opts.cy + opts.r2 * Math.sin(opts.startRadians),
          ],
          [
            opts.cx + opts.r2 * Math.cos(opts.closeRadians),
            opts.cy + opts.r2 * Math.sin(opts.closeRadians),
          ],
          [
            opts.cx + opts.r1 * Math.cos(opts.closeRadians),
            opts.cy + opts.r1 * Math.sin(opts.closeRadians),
          ],
          [
            opts.cx + opts.r1 * Math.cos(opts.startRadians),
            opts.cy + opts.r1 * Math.sin(opts.startRadians),
          ],
        ]

        var a = pt[0][0] - pt[1][0]
        var b = pt[0][1] - pt[1][1]
        var outer = Math.sqrt(a * a + b * b)

        var d = pt[2][0] - pt[3][0]
        var e = pt[2][1] - pt[3][1]
        var inner = Math.sqrt(d * d + e * e)

        let px = [
          opts.cx +
            opts.r2 *
              Math.cos(
                opts.startRadians +
                  Number(this.annularOptions.padding) * 0.0174533,
              ),
          opts.cx +
            opts.r2 *
              Math.cos(
                opts.closeRadians -
                  Number(this.annularOptions.padding) * 0.0174533,
              ),
          opts.cx +
            opts.r1 *
              Math.cos(
                opts.closeRadians -
                  ((Number(this.annularOptions.padding) * outer) / inner) *
                    0.0174533,
              ),
          opts.cx +
            opts.r1 *
              Math.cos(
                opts.startRadians +
                  ((Number(this.annularOptions.padding) * outer) / inner) *
                    0.0174533,
              ),
        ]

        for (let i = 0; i < 20; i++) {
          px.push(
            opts.cx +
              opts.r2 *
                Math.cos(
                  opts.startRadians +
                    ((opts.closeRadians - opts.startRadians) / 20) * i +
                    Number(this.annularOptions.padding) * 0.0174533,
                ),
          )
        }

        let py = [
          opts.cy +
            opts.r2 *
              Math.sin(
                opts.startRadians +
                  Number(this.annularOptions.padding) * 0.0174533,
              ),
          opts.cy +
            opts.r2 *
              Math.sin(
                opts.closeRadians -
                  Number(this.annularOptions.padding) * 0.0174533,
              ),
          opts.cy +
            opts.r1 *
              Math.sin(
                opts.closeRadians -
                  ((Number(this.annularOptions.padding) * outer) / inner) *
                    0.0174533,
              ),
          opts.cy +
            opts.r1 *
              Math.sin(
                opts.startRadians +
                  ((Number(this.annularOptions.padding) * outer) / inner) *
                    0.0174533,
              ),
        ]

        for (let i = 0; i < 20; i++) {
          py.push(
            opts.cy +
              opts.r2 *
                Math.sin(
                  opts.startRadians +
                    ((opts.closeRadians - opts.startRadians) / 20) * i +
                    Number(this.annularOptions.padding) * 0.0174533,
                ),
          )
        }
        //console.log(pt[0], ptc[0])
        let xPos =
          (this.draggingMarker.options.lastClientX -
            this.getBoardPosition().tx) /
            this.getBoardPosition().zoom -
          Number(this.annularOptions.size) / 2 +
          Math.min(...px) -
          6 // options.styles.padding
        let yPos =
          (this.draggingMarker.options.lastClientY -
            this.getBoardPosition().ty -
            60) /
            this.getBoardPosition().zoom -
          Number(this.annularOptions.size) / 2 +
          Math.min(...py) -
          6 // options.styles.padding
        let cWidth =
          Math.max(...px) - Math.min(...px) + 6 /* options.styles.padding */ * 2
        let cHeight =
          Math.max(...py) - Math.min(...py) + 6 /* options.styles.padding */ * 2
        Object.assign(this.draggingMarker.options, {
          x: xPos,
          y: yPos,
          width: cWidth,
          height: cHeight,
          annularPadding: Number(this.annularOptions.padding),
          centerPoint: Number(this.annularOptions.size) / 2,
          startDegrees: i * (360 / Number(this.annularOptions.pieces)),
          endDegrees: (i + 1) * (360 / Number(this.annularOptions.pieces)),
          innerRadius: Number(this.annularOptions.innerRadius),
          outerRadius: Number(this.annularOptions.outerRadius),
        })
        // window['ECOMAP_BOARD'].addMarkerToBoard(null, this.draggingMarker.options, this.draggingMarker.id)
        this.$store.dispatch('board/addMarker', this.draggingMarker.options)
        this.annularDialog = false
      }
    },
    startMarkerDrag(marker, event) {
      event.preventDefault()
      this.draggingMarker.isDragging = true
      window.addEventListener('mousemove', this.onMarkerDrag, false)
      window.addEventListener('mouseup', this.stopMarkerDrag, false)
      this.draggingMarker.options = null
      this.draggingMarker.options = JSON.parse(JSON.stringify(marker))
      this.draggingMarker.el = event.target.firstChild.cloneNode(true)
      Object.assign(this.draggingMarker.el.style, {
        position: 'absolute',
        zIndex: '10000',
        width: '64px',
        height: '64px',
        transition: 'none',
        cursor: 'grab',
        left: `${event.pageX - 32}px`,
        top: `${event.pageY - 32}px`,
      })
      document.body.appendChild(this.draggingMarker.el)
    },
    onMarkerDrag(event) {
      if (this.draggingMarker.isDragging) {
        Object.assign(this.draggingMarker.el.style, {
          left: `${event.pageX - 32}px`,
          top: `${event.pageY - 32}px`,
        })
      }
    },
    stopMarkerDrag(event) {
      this.draggingMarker.isDragging = false
      this.draggingMarker.el.remove()
      Object.assign(this.draggingMarker.options, {
        lastClientX: event.clientX,
        lastClientY: event.clientY,
        x:
          (event.clientX - this.getBoardPosition().tx) /
            this.getBoardPosition().zoom -
          this.draggingMarker.options.width / 2,
        y:
          (event.clientY - this.getBoardPosition().ty - 60) /
            this.getBoardPosition().zoom -
          this.draggingMarker.options.height / 2, // 60 is board offset on Y-axis
        scaleX: 1,
        scaleY: 1,
        styles: {
          padding: 6,
          background: true,
          fillColor: 'rgb(246,156,42)',
          strokeWidth: 4,
          editableText: 'Type Something',
        },
      })
      // check if it is in valid region
      const container = document.querySelector('.board-container')
      const screen = container.getBoundingClientRect()
      if (
        event.clientX + 50 < screen.x ||
        event.clientY + 50 < screen.y ||
        event.clientX + 50 > screen.x + screen.width ||
        event.clientY + 50 > screen.y + screen.height
      ) {
        return this.$notify.error({
          title: 'Not inside',
          message: `Drag [${this.draggingMarker.options.type}] inside the board to add it`,
        })
      }
      // this.$store.commit('board/setActiveMarker', this.draggingMarker)
      if (this.draggingMarker.options.type === 'annular') {
        this.annularDialog = true
      } else {
        console.log('activeMarker => ', this.$store.state.activeMarker)
        // window['ECOMAP_BOARD'].addMarkerToBoard(event, this.draggingMarker.options, this.draggingMarker.id)
        this.$store.dispatch('board/addMarker', this.draggingMarker.options)
      }
      window.removeEventListener('mousemove', this.onMarkerDrag, false)
      window.removeEventListener('mouseup', this.stopMarkerDrag, false)
    },
  },
}
