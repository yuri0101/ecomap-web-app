export default {
  data: () => ({
    oldPosition: null,
    holding: {
      position: {
        x: 0, y: 0
      },
      angle: 0,
      selected: true,
      disableScale: true,
      width: 300,
      height: 300,
    },
  }),
  mounted() {
    window.addEventListener('mousemove', this.onDrag, true)
    window.addEventListener('mouseup', this.stopDrag, true)
  },
  destroyed() {
    window.removeEventListener('mousemove', this.onDrag, true)
    window.removeEventListener('mouseup', this.stopDrag, true)
  },
  methods: {
    setHoldingPosition(e) {
      const bound = this.$refs['rect-marker-tool'].$el.getBoundingClientRect()
      const left = bound.left + (e.pageX - this.holding.position.x + 7)
      const top = bound.top + (e.pageY - this.holding.position.y + 6)
      // const node = this.$el.querySelector('.rect-marker-tool')
      // node.style.left = left + 'px'
      // node.style.top = top + 'px'
    },
    async startDrag(evt, shape) {
      if (evt.which !== 1) return
      console.log('startDrag', evt, shape)
      this.$set(this, 'holding', {
        type: shape,
        position: {
          x: 0, y: 0
        },
        angle: 0,
        selected: true,
        disableScale: true,
        width: 300,
        height: 300,
      })
      await this.$nextTick()
      this.holding.position = {
        x: evt.pageX,
        y: evt.pageY,
      }
      const bound = this.$refs['rect-marker-tool'].$el.getBoundingClientRect()
      this.oldPosition = {
        x: bound.x,
        y: bound.y,
      }
      this.setHoldingPosition(evt)
    },
    onDrag(evt) {
      if (!this.holding) return
      this.setHoldingPosition(evt)
    },
    stopDrag(evt) {
      if (!this.holding) return
      const node = this.holding
      const oldPosition = this.oldPosition
      this.$set(this, 'holding', null)
      const bound = this.$refs['rect-marker-tool'].$el.getBoundingClientRect()
      node.position.x = bound.x + evt.pageX - node.position.x
      node.position.y = bound.y + evt.pageY - node.position.y
      
      // check if it is in valid region
      const container = document.querySelector('#ecomap-board-1')
      const screen = container.getBoundingClientRect()

      if(node.position.x === oldPosition.x && node.position.y === oldPosition.y){
        this.$store.commit('showSmallModal', node)
        this.$store.commit('SmallInBoard', false)
      }
      
      if (node.position.x + 50 < screen.x ||
        node.position.y + 50 < screen.y ||
        node.position.x + 50 > screen.x + screen.width ||
        node.position.y + 50 > screen.y + screen.height ||
        node.position.x + 50 > screen.x + screen.width - 350
        ) {
        return this.$notify.error({
          title: 'Not inside',
          message: `Drag [${node.name}] inside the board to add it`,
        })
      }
      console.log('node', node)
      this.$store.dispatch('board/addMarker', node)
    },
  },
}
