import { mapGetters } from 'vuex'

const zoomSpeed = 0.1
const maxZoom = 3.0
const minZoom = 0.15

export default {
  data: () => ({
    mousePos: null,
    transform: null,
    coordinates: {
      x: 0, y: 0, zoom: 100,
    },
  }),
  computed:{
    ...mapGetters({
       enableSelection: 'enableSelection',
       modules: 'board/modules',
       sidebarVisible: 'sidebarVisible',
    }),
    isShared() {
      return  this.$route.query.share !== undefined
    },
  } ,
  watch: {
    transform({ x, y, zoom }, old) {
      if (old) {
        const { oldX, oldY, oldZoom } = old
        if (x === oldX && y === oldY && zoom === oldZoom) return
      }
      this.coordinates.zoom = Math.round(zoom * 100)
      this.instance && this.instance.setZoom(zoom)
      if (!this.$refs.board) return
      this.$refs.board.style.transformOrigin = '0 0 0'
      this.$refs.board.style.transform = `matrix(${zoom}, 0, 0, ${zoom}, ${x}, ${y})`
    },
    mousePos(cur, old) {
      if (this.enableSelection) return
      if (!this.$refs.board) return
      if (cur && !old) {
        this.$refs.board.style.cursor = 'move'
        this.$refs.board.style.transition = null
      }
      if (!cur && old) {
        this.$refs.board.style.cursor = 'default'
        this.$refs.board.style.transition = 'transform 0.25s ease'
      }
    },
  },
  methods: {
    async initializePanzoom() {
      // init transformation
      this.resetZoom()
      if (this.$refs.board) {
        this.$refs.board.style.transition = 'transform 0s ease'
      }

      const owner = this.$(this.$refs.board)

      // movements
      const startMovement = (x, y) => {
        if (this.enableSelection) {
          this.startSelect(x, y)
        } else {
          this.startPan(x, y)
        }
      }
      const moving = (x, y) => {
        // this.updateCoordinates(x, y)
        if (this.enableSelection) {
          this.onSelect(x, y)
        } else {
          this.onPan(x, y)
        }
      }
      const stopMovement = () => {
        if (this.enableSelection) {
          this.stopSelect()
        } else {
          this.stopPan()
        }
      }

      // Zoom on wheel
      owner.on('wheel', this.onWheel)

      // Keyboard shortcuts
      window.addEventListener('keyup', this.onKeyUp)
      window.addEventListener('keydown', this.onKeyDown)

      // Pan & select on mouse move
      owner.on('mousedown', e => {
        if (e.which !== 1) return
        startMovement(e.pageX, e.pageY)
      })
      owner.on('mousemove', e => {
        moving(e.pageX, e.pageY)
      })
      owner.on('mouseleave', stopMovement)
      owner.on('mouseup', stopMovement)

      // Pan & select on touch events
      owner.on('touchstart', e => {
        const touch = e.originalEvent.touches[0]
        if (touch) startMovement(touch.pageX, touch.pageY)
      })
      owner.on('touchmove', e => {
        const touch = e.originalEvent.touches[0]
        if (touch) moving(touch.pageX, touch.pageY)
      })
      owner.on('touchend', stopMovement)
      owner.on('touchcancel', stopMovement)

    },

    onKeyUp(e) {
      if (e.key === 'Control') {
        this.$store.commit('enableSelection', false)
      }
    },
    onKeyDown(e) {
      if (e.key === 'Control') {
        this.$store.commit('enableSelection', true)
      }
    },

    //
    // Selection control
    //
    startSelect(x, y) {
      // fix offset
      x = (x - this.transform.x) / this.transform.zoom - this.$el.offsetLeft
      y = (y - this.transform.y) / this.transform.zoom - this.$el.offsetTop
      // begin selection
      this.$set(this, 'mousePos', { x, y })
      this.$('#selection-control').css({
        display: 'block',
        top: y + 'px',
        left: x + 'px',
        width: 1 + 'px',
        height: 1 + 'px',
      })
    },
    onSelect(x, y) {
      if (!this.mousePos) return
      // fix offset
      x = (x - this.transform.x) / this.transform.zoom - this.$el.offsetLeft
      y = (y - this.transform.y) / this.transform.zoom - this.$el.offsetTop
      // update selection
      this.$('#selection-control').css({
        top: Math.min(this.mousePos.y, y) + 'px',
        left: Math.min(this.mousePos.x, x) + 'px',
        width: Math.abs(this.mousePos.x - x) + 'px',
        height: Math.abs(this.mousePos.y - y) + 'px',
      })
    },
    stopSelect() {
      if (this.mousePos) {
        const $control = this.$('#selection-control')
        const x = parseInt($control.css('left'), 10)
        const y = parseInt($control.css('top'), 10)
        const width = parseInt($control.css('width'), 10)
        const height = parseInt($control.css('height'), 10)
        this.$store.dispatch('board/createGroup', {
          x1: x,
          y1: y,
          x2: x + width,
          y2: y + height,
        }).then(val => {
          this.$store.commit('enableSelection', !val)
        })
      }
      this.$set(this, 'mousePos', null)
      this.$('#selection-control').css('display', 'none')
    },

    //
    // Pan control
    //
    startPan(x, y) {
      this.$set(this, 'mousePos', { x, y })
    },
    onPan(x, y) {
      if (!this.mousePos) return
      this.moveTo(
        this.transform.x + x - this.mousePos.x,
        this.transform.y + y - this.mousePos.y,
      )
      this.$set(this, 'mousePos', { x, y })
    },
    stopPan() {
      this.$set(this, 'mousePos', null)
    },

    //
    // Zoom control
    //
    onWheel(e) {
      e.preventDefault()
      if (this.mousePos) return
      const delta = e.wheelDelta || e.originalEvent.wheelDelta
      let zoom = this.transform.zoom
      if (delta > 0) zoom += zoomSpeed
      if (delta < 0) zoom -= zoomSpeed
      let mainBoardOffset = this.$('.main-board').offset()
      // console.log(mainBoardOffset)
      this.zoomTo(e.clientX - mainBoardOffset.left, e.clientY - mainBoardOffset.top, zoom)
    },

    applyTransform(x, y, zoom) {
      // keep inside of bound
      if (this.transform) {
        if (!this.$refs.board) return
        const ownerBound = this.$el.getBoundingClientRect()
        const right = ownerBound.left + x + this.$refs.board.clientWidth * zoom
        const bottom = ownerBound.top + y + this.$refs.board.clientHeight * zoom
        if (right < ownerBound.right) {
          x += ownerBound.right - right
        }
        if (bottom < ownerBound.bottom) {
          y += ownerBound.bottom - bottom
        }
        x = Math.min(0, Math.round(x || 0))
        y = Math.min(0, Math.round(y || 0))
      }
      // apply new transform
      this.$set(this, 'transform', { x, y, zoom })
    },

    moveTo(x, y) {
      this.applyTransform(x, y, this.transform.zoom)
    },
    zoomTo(x, y, zoom) {
      // validate args
      const bound = this.$el.getBoundingClientRect()
      if (typeof x !== 'number') {
        x = bound.left + bound.width / 2
      }
      if (typeof y !== 'number') {
        y = bound.top + bound.height / 2
      }
      if (typeof zoom !== 'number') {
        zoom = 1
      }

      x -= this.transform.x
      y -= this.transform.y
      zoom = Math.max(zoom, minZoom)
      zoom = Math.min(zoom, maxZoom)

      // calculate position
      const delta = 1 - zoom / this.transform.zoom
      this.applyTransform(
        this.transform.x + x * delta,
        this.transform.y + y * delta,
        zoom,
      )
    },

    //
    // Others
    //
    updateCoordinates(x, y, zoom = 100) {
      const ownerBound = this.$el.getBoundingClientRect()
      x -= ownerBound.left
      y -= ownerBound.top
      if (this.transform) {
        if (!this.$refs.board) return
        x -= this.transform.x
        y -= this.transform.y
        x /= this.transform.zoom
        y /= this.transform.zoom
        x -= this.$refs.board.clientWidth * 0.5
        y -= this.$refs.board.clientHeight * 0.5
        x = Math.round(x / 5)
        y = Math.round(-y / 5)
        zoom = Math.round(this.transform.zoom * 100)
      }
      this.$set(this, 'coordinates', { x, y, zoom })
    },

    //
    // Accessible methods
    //
    zoomIn() {
      this.zoomTo(null, null, this.transform.zoom + 0.1)
    },
    zoomOut() {
      this.zoomTo(null, null, this.transform.zoom - 0.1)
    },
    resetZoom() {
      // console.log('====== resetZoom ======')
      const ownerBound = this.$el.getBoundingClientRect()
      let zoomVariable = this.sidebarVisible ? 350 : 0
      // console.log('====== zoomVariable ======', zoomVariable)
      let customOwnerBoundWidth = (ownerBound.width >= 960) ? ownerBound.width - zoomVariable : ownerBound.width
      if (this.modules.length == 0) {
        if (!this.$refs.board) return
        return this.applyTransform(
          -(this.$refs.board.clientWidth - customOwnerBoundWidth) / 2,
          -(this.$refs.board.clientHeight - ownerBound.height) / 2,
          1.0,
        )
      }
      const pad = 200
      let minX = Number.MAX_SAFE_INTEGER
      let minY = Number.MAX_SAFE_INTEGER
      let maxX = Number.MIN_SAFE_INTEGER
      let maxY = Number.MIN_SAFE_INTEGER
      for (const node of this.modules) {
        minX = Math.min(node.position.x - pad, minX)
        minY = Math.min(node.position.y - pad, minY)
        maxX = Math.max(node.position.x + pad, maxX)
        maxY = Math.max(node.position.y + pad, maxY)
      }
      for (const marker of this.markers) {
        minX = Math.min(marker.x - pad/2, minX)
        minY = Math.min(marker.y - pad/2, minY)
        maxX = Math.max(marker.x + pad/2, maxX)
        maxY = Math.max(marker.y + pad/2, maxY)
      }
      let zoom = Math.min(
        customOwnerBoundWidth / (maxX - minX),
        ownerBound.height / (maxY - minY),
      )
      let x = ((maxX + minX) * zoom - customOwnerBoundWidth) / 2
      let y = ((maxY + minY) * zoom - ownerBound.height) / 2
      this.applyTransform(-x, -y, zoom)
    },
  },
}
