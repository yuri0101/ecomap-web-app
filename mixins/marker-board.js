import { mapGetters } from 'vuex'
export default {
  computed: {
    ...mapGetters({
      sidebarVisible: 'sidebarVisible',
    }),
  },
  methods: {
    calculatePositions(align, position, countPoint) {
      let positions = [], minRadius, width, height
      const minGap = (this.moduleRadius + 10) * 2 + 10
      const textGap = 20
      if (countPoint <= 10) {
        minRadius = minGap * 2.5
        switch (align) {
          case 'rect': {
              [...new Array(Math.ceil(countPoint / 2))].forEach((e, i) => {
                positions.push({
                  x: position.x + minGap / 2,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
              })
              width = minGap * 2
              height = (minGap + textGap) * Math.ceil(countPoint / 2) + 30
            }
            break
          case 'circle': {
              [...new Array(Math.ceil(countPoint))].forEach((e, i) => {
                positions.push({
                  x: position.x + minRadius + 100 + minRadius * Math.cos(i * 2 * Math.PI / countPoint),
                  y: position.y + minRadius + 100 - minRadius * Math.sin(i * 2 * Math.PI / countPoint),
                })
              })
              width = (minRadius + 100) * 2
              height = (minRadius + 100) * 2
            }
            break
          case 'pan': {
              [...new Array(Math.ceil(countPoint / 2))].forEach((e, i) => {
                positions.push({
                  x: position.x + minGap / 2,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
              })
              width = minGap * 2
              height = (minGap + textGap) * Math.ceil(countPoint / 2) + 30
            }
            break
        }
      } else if (countPoint <= 100) {
        minRadius = minGap * 3
        switch (align) {
          case 'rect': {
              [...new Array(Math.ceil(countPoint / 5))].forEach((e, i) => {
                positions.push({
                  x: position.x + minGap / 2,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 2,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 3,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 4,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
              })
              width = minGap * 5
              height = (minGap + textGap) * Math.ceil(countPoint / 5) + 30
            }
            break
          case 'circle': {
              [...new Array(Math.ceil(countPoint / 3))].forEach((e, i) => {
                positions.push({
                  x: position.x + minRadius * 2 + 100 + minRadius * Math.cos(i * 2 * Math.PI / countPoint * 3),
                  y: position.y + minRadius * 2 + 100 - minRadius * Math.sin(i * 2 * Math.PI / countPoint * 3),
                })
                positions.push({
                  x: position.x + minRadius * 2 + 100 + minRadius * 1.5 * Math.cos(i * 2 * Math.PI / countPoint * 3),
                  y: position.y + minRadius * 2 + 100 - minRadius * 1.5 * Math.sin(i * 2 * Math.PI / countPoint * 3),
                })
                positions.push({
                  x: position.x + minRadius * 2 + 100 + minRadius * 2 * Math.cos(i * 2 * Math.PI / countPoint * 3),
                  y: position.y + minRadius * 2 + 100 - minRadius * 2 * Math.sin(i * 2 * Math.PI / countPoint * 3),
                })
              })
              width = (minRadius * 2 + 100) * 2
              height = (minRadius * 2 + 100) * 2 + 30
            }
            break
          case 'pan': {
              [...new Array(Math.ceil(countPoint / 5))].forEach((e, i) => {
                positions.push({
                  x: position.x + minGap / 2,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 2,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 3,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 4,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
              })
              width = minGap * 5
              height = minGap * Math.ceil(countPoint / 5) + 30
            }
            break
        }
      } else {
        minRadius = minGap * 4
        switch (align) {
          case 'rect': {
              [...new Array(Math.ceil(countPoint / 10))].forEach((e, i) => {
                positions.push({
                  x: position.x + minGap / 2,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 2,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 3,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 4,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 5,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 6,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 7,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 8,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 9,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
              })
              width = minGap * 10
              height = (minGap + textGap) * Math.ceil(countPoint / 10) + 30
            }
            break
          case 'circle': {
              [...new Array(Math.ceil(countPoint / 10))].forEach((e, i) => {
                positions.push({
                  x: position.x + minRadius * 4.2 + minRadius * Math.cos(i * 2 * Math.PI / countPoint),
                  y: position.y + minRadius * 4.2 - minRadius * Math.sin(i * 2 * Math.PI / countPoint),
                })
                positions.push({
                  x: position.x + minRadius * 4.2 + minRadius * 1.5 * Math.cos(i * 2 * Math.PI / countPoint),
                  y: position.y + minRadius * 4.2 - minRadius * 1.5 * Math.sin(i * 2 * Math.PI / countPoint),
                })
                positions.push({
                  x: position.x + minRadius * 4.2 + minRadius * 2 * Math.cos(i * 2 * Math.PI / countPoint),
                  y: position.y + minRadius * 4.2 - minRadius * 2 * Math.sin(i * 2 * Math.PI / countPoint),
                })
                positions.push({
                  x: position.x + minRadius * 4.2 + minRadius * 2.5 * Math.cos(i * 2 * Math.PI / countPoint),
                  y: position.y + minRadius * 4.2 - minRadius * 2.5 * Math.sin(i * 2 * Math.PI / countPoint),
                })
                positions.push({
                  x: position.x + minRadius * 4.2 + minRadius * 2.9 * Math.cos(i * 2 * Math.PI / countPoint),
                  y: position.y + minRadius * 4.2 - minRadius * 2.9 * Math.sin(i * 2 * Math.PI / countPoint),
                })
                positions.push({
                  x: position.x + minRadius * 4.2 + minRadius * 3.2 * Math.cos(i * 2 * Math.PI / countPoint),
                  y: position.y + minRadius * 4.2 - minRadius * 3.2 * Math.sin(i * 2 * Math.PI / countPoint),
                })
                positions.push({
                  x: position.x + minRadius * 4.2 + minRadius * 3.5 * Math.cos(i * 2 * Math.PI / countPoint),
                  y: position.y + minRadius * 4.2 - minRadius * 3.5 * Math.sin(i * 2 * Math.PI / countPoint),
                })
                positions.push({
                  x: position.x + minRadius * 4.2 + minRadius * 3.8 * Math.cos(i * 2 * Math.PI / countPoint),
                  y: position.y + minRadius * 4.2 - minRadius * 3.8 * Math.sin(i * 2 * Math.PI / countPoint),
                })
                positions.push({
                  x: position.x + minRadius * 4.2 + minRadius * 4 * Math.cos(i * 2 * Math.PI / countPoint),
                  y: position.y + minRadius * 4.2 - minRadius * 4 * Math.sin(i * 2 * Math.PI / countPoint),
                })
                positions.push({
                  x: position.x + minRadius * 4.2 + minRadius * 4.2 * Math.cos(i * 2 * Math.PI / countPoint),
                  y: position.y + minRadius * 4.2 - minRadius * 4.2 * Math.sin(i * 2 * Math.PI / countPoint),
                })
              })
              width = (minRadius * 4.2 + 100) * 2
              height = (minRadius * 4.2 + 100) * 2 + 30
            }
            break
          case 'pan': {
              [...new Array(Math.ceil(countPoint / 10))].forEach((e, i) => {
                positions.push({
                  x: position.x + minGap / 2,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 2,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 3,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 4,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 5,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 6,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 7,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 8,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
                positions.push({
                  x: position.x + minGap / 2 + minGap * 9,
                  y: position.y + minGap / 2 + (minGap + textGap) * i,
                })
              })
              width = minGap * 10
              height = (minGap + textGap) * Math.ceil(countPoint / 10) + 30
            }
            break
        }
      }
      return { positions, width, height }
    },
    // ------------------------------------------------------------------------ //
    dblClick(event) {
      console.log('dblClick')
      this.$store.dispatch('board/updateMarkers', {
        id: event.id,
        payload: {
          editable: true,
          selected: false,
        },
      })
      this.$nextTick().then(function () {
        setTimeout(function () {
          event.textEl.focus()
          placeCaretAtEnd(event.textEl)
        }, 1000)
      })
      function placeCaretAtEnd(el) {
        if (
          typeof window.getSelection != 'undefined' &&
          typeof document.createRange != 'undefined'
        ) {
          let range = document.createRange()
          range.selectNodeContents(el)
          range.collapse(false)
          let sel = window.getSelection()
          sel.removeAllRanges()
          sel.addRange(range)
        } else if (typeof document.body.createTextRange != 'undefined') {
          let textRange = document.body.createTextRange()
          textRange.moveToElementText(el)
          textRange.collapse(false)
          textRange.select()
        }
      }
    },
    getBoardPosition() {
      let tm = document.getElementById('ecomap-board').style.transform
      tm = tm.slice('matrix('.length, -1)
      tm = tm.split(',').map(Number)
      return {
        zoom: tm[0],
        tx: tm[4],
        ty: tm[5],
      }
    },
    textChanged(options) {
      console.log('textChanged')
      this.$store.dispatch('board/updateMarkers', {
        id: options.id,
        payload: {
          styles: {
            padding: options.styles.padding ? options.styles.padding : 6,
            background: options.styles.background
              ? options.styles.background
              : true,
            fillColor: options.styles.fillColor
              ? options.styles.fillColor
              : 'rgb(246,156,42)',
            strokeWidth: options.styles.strokeWidth
              ? options.styles.strokeWidth
              : 4,
            editableText: options.textEl.innerText,
          },
          width:
            options.type === 'textbox'
              ? options.width
              : options.textEl.getBoundingClientRect().width /
                options.scaleX /
                this.getBoardPosition().zoom,
          height:
            options.textEl.getBoundingClientRect().height /
            options.scaleY /
            this.getBoardPosition().zoom,
        },
      })
      this.$store.commit('board/setTextChanged', false)
    },
    selectMarker(event) {
      // console.log('selectMarker', event)
      this.$store.commit('updateActiveTab', 1)
      if (!this.sidebarVisible) {
        this.$store.dispatch('toggleSidebar')
      }
      console.log('selectMarker')
      // this.$store.commit('board/isDragging', true)
      this.$store.commit('board/incrementCtr')
      this.$store.dispatch('board/updateMarkers', {
        id: event,
        payload: {
          selected: true,
          editable: false,
        },
      })
    },
    updateSelection() {
      console.log('updateSelection')
      // if(this.$store.state.showSmallModal) this.$store.commit('showSmallModal', null)
      this.$store.dispatch('board/updateMarkers', {
        id: 'all',
        payload: {
          selected: false,
          editable: false,
        },
      })
    },
    updateMarker(id, payload) {
      console.log('updateMarker', id, payload)
      this.$store.commit('board/isDragging', true)
      this.$store.commit('board/incrementCtr')
      this.$store.dispatch('board/updateMarkers', {
        id: id,
        payload: payload,
      })
    },
    getElementStyles(element) {
      const styles = element.styles ? element.styles : {}
      return {
        width: `${element.width}px`,
        height: `${element.height}px`,
        ...styles,
      }
    },
  },
}
