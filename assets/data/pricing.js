export default {
  featureList: [
    'Gain insights into ecosystems',
    'Access stakeholder information',
    'Create, share, embed anonymously',
    'No ads on ecosystem maps',
    // --------------------------- //
    'Modify ecosystem maps',
    'Add and edit stakeholders',
    'Create multiple ecosystem maps',
    'Community membership',
    // --------------------------- //
    'Create more than 3 maps',
    'Unlimited private maps',
    'More stakeholder information',
    // --------------------------- //
    'A researcher for 2h / week',
    'Premium support',
    'White-label product',
  ],
  guest: {
    title: 'Guest',
    payment: 'USD $0.00/month',
    features: 4,
  },
  free: {
    title: 'Free',
    payment: 'USD $0.00/month',
    features: 8,
  },
  premium: {
    title: 'Premium',
    payment: 'USD $99/month',
    features: 11,
  },
  enterprise: {
    title: 'Enterprise',
    payment: 'Custom',
    features: 14,
  },
}
