export default {
  '1': [
    {
      id: '1',
      fill: '#F44336',
      hover: '#E53935',
      anchor: {
        active: {
          'square': [ 1.175, 0.420, 0, 0 ],
          'circle': [ 1.175, 0.42, 0, 0 ],
        },
        inactive: {
          'square': [ 0.9, 0.420, 0, 0 ],
          'circle': [ 0.9, 0.420, 0, 0 ],
        },
      },
    },
  ],
  '2': [
    {
      id: '1',
      fill: '#66BB6A',
      hover: '#4CAF50',
      anchor: {
        active: {
          'square': [ 1.175, 0.14, 0, 0 ],
          'circle': [ 1.1, 0.14, 0, 0 ],
        },
        inactive: {
          'square': [ 0.9, 0.14, 0, 0 ],
          'circle': [ 0.8, 0.14, 0, 0 ],
        },
      },
    },
    {
      id: '2',
      fill: '#26A69A',
      hover: '#009688',
      anchor: {
        active: {
          'square': [ 1.175, 0.420, 0, 0 ],
          'circle': [ 1.175, 0.42, 0, 0 ],
        },
        inactive: {
          'square': [ 0.9, 0.332, 0, 0 ],
          'circle': [ 0.9, 0.332, 0, 0 ],
        },
      },
    },
  ],
  '3': [
    {
      id: '1',
      fill: '#42A5F5',
      hover: '#2196F3',
      anchor: {
        active: {
          'square': [ 1.175, 0.14, 0, 0 ],
          'circle': [ 1.1, 0.14, 0, 0 ],
        },
        inactive: {
          'square': [ 0.9, 0.14, 0, 0 ],
          'circle': [ 0.8, 0.14, 0, 0 ],
        },
      },
    },
    {
      id: '2',
      fill: '#66BB6A',
      hover: '#4CAF50',
      anchor: {
        active: {
          'square': [ 1.175, 0.420, 0, 0 ],
          'circle': [ 1.175, 0.42, 0, 0 ],
        },
        inactive: {
          'square': [ 0.9, 0.332, 0, 0 ],
          'circle': [ 0.9, 0.332, 0, 0 ],
        },
      },
    },
    {
      id: '3',
      fill: '#26A69A',
      hover: '#009688',
      anchor: {
        active: {
          'square': [ 1.175, 0.715, 0, 0 ],
          'circle': [ 1.1, 0.715, 0, 0 ],
        },
        inactive: {
          'square': [ 0.9, 0.524, 0, 0 ],
          'circle': [ 0.9, 0.524, 0, 0],
        },
      },
    },
  ],
  '4': [
    {
      id: '1',
      fill: '#F44336',
      hover: '#E53935',
      anchor: {
        active: {
          'square': [ 1.175, 0.14, 0, 0 ],
          'circle': [ 1.1, 0.14, 0, 0 ],
        },
        inactive: {
          'square': [ 0.9, 0.14, 0, 0 ],
          'circle': [ 0.8, 0.14, 0, 0 ],
        },
      },
    },
    {
      id: '2',
      fill: '#66BB6A',
      hover: '#4CAF50',
      anchor: {
        active: {
          'square': [ 1.175, 0.420, 0, 0 ],
          'circle': [ 1.175, 0.42, 0, 0 ],
        },
        inactive: {
          'square': [ 0.9, 0.332, 0, 0 ],
          'circle': [ 0.9, 0.332, 0, 0 ],
        },
      },
    },
    {
      id: '3',
      fill: '#26A69A',
      hover: '#009688',
      anchor: {
        active: {
          'square': [ 1.175, 0.715, 0, 0 ],
          'circle': [ 1.1, 0.715, 0, 0 ],
        },
        inactive: {
          'square': [ 0.9, 0.524, 0, 0 ],
          'circle': [ 0.9, 0.524, 0, 0],
        },
      },
    },
    {
      id: '4',
      fill: '#42A5F5',
      hover: '#2196F3',
      anchor: {
        active: {
          'square': [ 1.175, 1, 0, 0 ],
          'circle': [ 0.85, 0.94, 0, 0 ],
        },
        inactive: {
          'square': [ 0.9, 0.715, 0, 0 ],
          'circle': [ 0.8, 0.715, 0, 0 ],
        },
      },
    },
  ],
}
