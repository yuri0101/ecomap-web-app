export default {
  title: 'Auto Generated',
  details: 'Auto-generated market map',
  titleCenter: 'Market Map',
  groups: [
    {
      'position': {
        'x': 3660,
        'y': 4454,
      },
    },
    {
      'position': {
        'x': 4275,
        'y': 4454,
      },
    },
    {
      'position': {
        'x': 4890,
        'y': 4454,
      },
    },
    {
      'position': {
        'x': 3660,
        'y': 4915,
      },
    },
    {
      'position': {
        'x': 4890,
        'y': 4915,
      },
    },
    {
      'position': {
        'x': 3660,
        'y': 5376,
      },
    },
    {
      'position': {
        'x': 4275,
        'y': 5376,
      },
    },
    {
      'position': {
        'x': 4890,
        'y': 5376,
      },
    },
  ],
  marker: {
    'type': 'title',
    'angle': 0,
    'width': 400,
    'height': 55,
    'scaleX': 1,
    'scaleY': 1,
    'styles': {
      'padding': 6,
      'fillColor': '#2196F3',
      'background': true,
      'strokeWidth': 4,
      'editableText': '',
    },
    'editable': false,
    'aspectRatio': true,
    'disableScale': false,
  },
  markerCenter: {
    'x': 4480,
    'y': 4950,
    'id': 'centerText',
    'type': 'title',
    'angle': 0,
    'width': 437,
    'height': 110,
    'scaleX': 1.7,
    'scaleY': 1.7,
    'styles': {
      'padding': 6,
      'fillColor': '#3F51B5',
      'background': true,
      'strokeWidth': 3,
      'editableText': '',
    },
    'editable': false,
    'aspectRatio': true,
    'disableScale': false,
  },
}
